<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Machan</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                <a href="http://www.machangroup.com.tw/"><img src="/img/machan_logo.png" width="50%" /></a>
            </div>

            <div class="row" align="center">
                <select id="selectProfile" class="form-control" style="width: 40%;" onchange="directPage()">
                    <option value="">請選擇看板線群</option>
                    <option value="01100004A001">一群裝配A線</option>
                    <option value="01100004B001">一群裝配B線</option>
                    <option value="01100004C001">一群裝配C線</option>
                    <option value="01100204A001">二群裝配A線</option>
                    <option value="01100204B001">二群裝配B線</option>
                    <option value="01100204C001">二群裝配C線</option>
                    <option value="01100304A001">三群裝配A線</option>
                    <option value="01100304B001">三群裝配B線</option>
                    <option value="01100304C001">三群裝配C線</option>
                    <option value="01100504A001">五群裝配A線</option>
                    <option value="01100504B001">五群裝配B線</option>
                    <option value="01100504C001">五群裝配C線</option>
                </select>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    profile = document.getElementById('selectProfile');

    directPage = () => {
        localStorage["profile"] = profile.options[profile.selectedIndex].value;
        localStorage["name"] = profile.options[profile.selectedIndex].text;
        window.location.href = "/dashboard";
    }

</script>
</html>

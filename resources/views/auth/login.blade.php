@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">登入</div>

                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="account" class="col-md-4 control-label">帳號</label>

                            <div class="col-md-6">
                                <input id="account" type="text" class="form-control" name="account" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">密碼</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="login()">
                                    登入
                                </button>

                                <a class="btn btn-link" href="">
                                    忘記密碼?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script>
    const login = () => {
        event.preventDefault()
        var account = document.getElementById('account').value
        var password = document.getElementById('password').value
        $.ajax({
            url: '/login',
            dataType: 'JSON',
            type: 'POST',
            data: {
                account: account,
                password: password
            },
            success: data => {
                window.location.href = '/'
            },
            statusCode: {
                401: result => {
                    alert('帳號或密碼錯誤')
                }
            }
        })
    }
</script>
@endsection

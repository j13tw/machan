<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="/js/jquery-3.1.1.min.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    @yield('myStyle')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/img/machan_logo.png" width="15%">
                    </a>
                </div>
                <div class="row" align="center">
                    <select id="selectProfile" class="form-control" style="width: 40%;" onchange="changeProfile()">
                        <option value="none">請選擇看板線群</option>
                        <option value="01100004A001">一群裝配A線</option>
                        <option value="01100004B002">一群裝配B線</option>
                        <option value="01100004B003">一群裝配C線</option>
                        <option value="01100204A004">二群裝配A線</option>
                        <option value="01100204B005">二群裝配B線</option>
                        <option value="01100204B006">二群裝配C線</option>
                        <option value="01100304A007">三群裝配A線</option>
                        <option value="01100504A008">五群裝配A線</option>
                        <option value="01100504B009">五群裝配B線</option>
                    </select>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
@yield('myScript')
</html>

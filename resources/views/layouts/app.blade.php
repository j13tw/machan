<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Machan</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
      html, body {
          font-family: 'Raleway', sans-serif, Microsoft JhengHei;
          width: 100%;
          height: 100%;
          margin: 0px;
          padding: 0px;
      }
      .appNavbar {
          height:50px;
      }
      .subTitle {
          float:left;font-family: '微軟正黑體 Bold', '微軟正黑體';
          font-weight: 700;
          font-style: normal;
          font-size: 20px;
          margin-top: 10px;
          float:left;
      }
      .navbarBtn {
          float:left;
          color:black;
          font-weight: bold;
          color:#636b6f;
          margin-left: 15px;
          font-size: 14px;
      }
      .navbarBtnGroup{
          float:left;
          margin-top: 8px;
      }
    </style>
    @yield('subPageCss')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color: black; font-size: 16pt;">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/logo.png') }}" height="30" width="233">
                    </a>
                    <div style="float: left; margin-left: 15px;">
                        <p class="subTitle" >明昌IOT自動報工系統<p>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @auth
                        <li>
                            <a href="/" style="color: #FFFFFF;">首頁</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #FFFFFF;">
                                報工管理 <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('mes/rfid') }}">RFID資料數據</a>
                                    <a href="{{ url('mes/mo/rfid') }}">製令RFID報工數據</a>
                                    <a href="{{ url('mes/recording') }}">錄入模組</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #FFFFFF;">
                                系統管理 <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('setup/unfinished') }}">尾數原因設定</a>
                                    <a href="{{ url('setup/exception') }}">異常原因設定</a>
                                    <a href="{{ url('setup/suspend') }}">除外原因設定</a>
                                    <a href="{{ url('device/tablet') }}">Tablet 參數設定</a>
                                    <a href="{{ url('device/board') }}">電子看板參數設定</a>
                                    <a href="{{ url('device/print') }}">藍芽印表機參數配對設定</a>
                                    <a href="{{ url('device/antenna') }}">Antenna 設備參數設定</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #FFFFFF;">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="/user/edit">
                                        修改個人資訊
                                    </a>
                                </li>
                                <li>
                                    <a href="/logout"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        登出
                                    </a>

                                    <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href="/login" style="color: #FFFFFF;">登入</a>
                        </li>
                        <li>
                            <a href="/register" style="color: #FFFFFF;">註冊</a>
                        </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('subPageJs')
</body>
</html>

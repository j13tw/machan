@extends('layouts.app')

@section('subPageCss')
  <style>
      table{
        table-layout: fixed;
        word-wrap:break-word;
      }
      .mainBtn{
        font-weight:Bold;
        color:black;
      }
      .searchInput{
        width:50px;
      }
      .modal-dialog {
        margin-top: 20vh;
      }
      #errorMsg {
        color: red;
      }
      #updateMsg {
        color: red;
      }
  </style>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:30px;font-weight:Bold;">
                    除外原因設定
                </div>
                
                <div class="modal bs-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modalStyle">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<h4 class="modal-title" id="exampleModalLabel" align="center">除外原因編輯</h4>
							</div>
							<div class="modal-body">
								<div class="panel-body">
									<table class="table table-bordered">
										<tr>
                                            <th class="info">序號</th>
											<th class="info">除外碼</th>
											<th class="info">除外原因</th>
										</tr>
                                        <tr>
                                            <td><input type="text" class="form-control" id="edit_id" readonly /></td>
                                            <td><input type="text" class="form-control" id="edit_code" readonly /></td>
                                            <td><input type="text" class="form-control" id="edit_suspend" /></td>
                                        </tr>
									</table>
                                    <span id="updateMsg"></span>
								</div>
                                <div class="panel-body">
                                    <button class="btn btn-warning" onclick="updateData()">更新</button>
                                </div>
							</div>
						</div>
					</div>
				</div>

                <div class="panel-body">
                    <table class="table table-bordered" id="tableSuspend">
                        <tr>
                            <th class="info">序號</th>
                            <th class="info">除外碼</th>
                            <th class="info">除外原因</th>
                            <th class="info">上次更新時間</th>
                            <th class="info">編輯</th>
                            <th class="info">刪除</th>
                        </tr>
                        @foreach ($suspendInfo as $key => $data)
                            <tr>
                                <td><span>{{ $data->id }}</span></td>
                                <td><span id="spCode-{{ $data->id }}">{{ $data->sp_code }}</span></td>
                                <td><span id="suspend-{{ $data->id }}">{{ $data->suspend }}</span></td>
                                @if (strtotime($data->created_at) !== strtotime($data->updated_at))
                                    <td><span id="updated-{{ $data->id }}">{{ $data->updated_at }}</span></td>
                                @else
                                    <td></td>
                                @endif
                                <td><button class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="editRow({{ $data }})">編輯</button></td>
                                <td><button class="btn btn-danger" onclick="deleteData({{ $data->id }})">刪除</button></td>
                            </tr>
                        @endforeach
                    </table>
                    <span id="errorMsg"></span>
                </div>

                <div class="panel-body">
                    <button class="btn btn-primary" onclick="addRow()">新增除外原因項目</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script type="text/javascript">
    var tbody = null
    var errMsg = document.getElementById('errorMsg')
    let updateMsg = document.getElementById('updateMsg')
    var rowStatus = 0
    
    addRow = () => {
        if (rowStatus === 0) {
            tbody = $('<tbody id="tbody"/>')
            rowStatus = 1
            tbody.append('<tr role="row">'
                +'<td class="active"><button class="btn btn-info" onclick="cancelAddRow()">取消</button></td>'
                +'<td class="active">'+'<input type="text" class="form-control" id="sp_code" />'+'</td>'
                +'<td class="active">'+'<input type="text" class="form-control" id="suspend" />'+'</td>'
                +'<td class="active"><button class="btn btn-info" onclick="storeData()">確認送出</button></td>'
                +'<td class="active"></td>'
                +'<td class="active"></td>'
            )

            $('#tableSuspend').append(tbody)
        }
    }

    cancelAddRow = () => {
        if (rowStatus === 1) {
            rowStatus = 0
            errMsg.innerHTML = ''
            tbody.remove()
        }
    }

    editRow = (data) => {
        document.getElementById('edit_id').value = data.id
        document.getElementById('edit_code').value = data.sp_code
        document.getElementById('edit_suspend').value = data.suspend
    }

    updateData = () => {
        var id = document.getElementById('edit_id').value
        if (confirm('確定進行更新？')) {
            $.ajax({
                url: '/setup/suspend/update/' + id,
                dataType: 'JSON',
                type: 'PUT',
                data: {
                    suspend: document.getElementById('edit_suspend').value
                },
                success: (response) => {
                    switch (response.status) {
                        case 0: {
                            window.location.reload()
                            break
                        }
                        case 1: {
                            updateMsg.innerHTML = '更新失敗'
                            break
                        }
                        case 2: {
                            updateMsg.innerHTML = '查無此資料'
                            break
                        }
                    }
                }
            })
        }
    }

    deleteData = (id) => {
        if (confirm('確定刪除資料？')) {
            $.ajax({
                url: '/setup/suspend/destroy/' + id,
                dataType: 'JSON',
                type: 'DELETE',
                success: (response) => {
                    switch (response.status) {
                        case 0: {
                            window.location.reload()
                            break
                        }
                        case 1: {
                            errMsg.innerHTML = '刪除失敗'
                            break
                        }
                        case 2: {
                            errMsg.innerHTML = '查無此資料'
                            break
                        }
                    }
                }
            })
        }
    }

    storeData = () => {
        if (checkSendData() === true) {
            $.ajax({
                url: '/setup/suspend/store',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    sp_code: spCode,
                    suspend: suspend
                },
                success: (response) => {
                    if (response.status === 0) {
                        window.location.reload()
                    } else {
                        errMsg.innerHTML = '新增失敗'
                    }
                },
                statusCode: {
                    422: (result) => {
                        var msg = JSON.parse(result.responseText).sp_code
                        errMsg.innerHTML = msg
                    }
                }
            })
        }
    }

    checkSendData = () => {
        spCode = document.getElementById('sp_code').value
        suspend = document.getElementById('suspend').value
        if (!spCode.trim() || !suspend.trim()) {
            alert('資料不允許為空')
        } else {
            return true
        }
    }
</script>
@endsection

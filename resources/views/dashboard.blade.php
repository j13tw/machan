@extends('layouts.master')

@section('content')
<div class="row" align="center">
    <span id="workStatus"></span>
</div>

<div id="accumulatorBoard" align="center">
    <div class="blue_block" style="font-size: 16pt;">
        <div id="profile"></div>
    </div>

    <div class="row">
        <div class="col-md-4" id="accu_1" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="font-size: 26pt;">現在時間</p>
                </div>
                <div align="center">
                    <p id="nowTime" style="color: #5599FF; font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
        </div>
        <div class="col-md-4" id="accu_2" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="font-size: 26pt;">累計投入數</p>
                </div>
                <div align="center">
                    <p id="accumulator_start" style="font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
        </div>
        <div class="col-md-4" id="accu_3" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="font-size: 26pt;">累計完工數</p>
                </div>
                <div align="center">
                    <p id="accumulator_fin" style="font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4" id="accu_4" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="font-size: 26pt;">目標約當量</p>
                </div>
                <div align="center">
                    <p id="target_qty" style="font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
            <div id="colData_1" style="font-size: 26pt;">
                <p id="mo_count">線別常駐指令數：-</p>
                <p id="finCount">已完成製令數：-</p>
            </div>
        </div>
        <div class="col-md-4" id="accu_5" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="font-size: 26pt;">累計實際約當量</p>
                </div>
                <div align="center">
                    <p id="accuRealQty" style="font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
            <div id="colData_2" style="font-size: 26pt;">
                <p id="accu_nf_qty">累計尾數：-</p>
                <p id="day_accu_pass_rate">當日累計一次通過率：-</p>
            </div>
        </div>
        <div class="col-md-4" id="accu_6" style="background-color: #CCFF99;">
            <div class="panel panel-default">
                <div style="background-color: #FFFF33;">
                    <p class="panel-title" align="center" style="color: red; font-size: 26pt;">產能達成率</p>
                </div>
                <div align="center">
                    <p id="qtyRate" style="font-size: 48pt; font-weight: bold;">-</p>
                </div>
            </div>
            <div id="colData_3" style="font-size: 26pt;">
                <p id="accu_change_time">累計換線時間：-</p>
                <p>累計除外時間：-</p>
            </div>
        </div>
    </div>
</div>

<div align="center">
    <div class="row" id="rowBoard" style="display: none; width: 100%;">
        <div class="col-md-4" id="col_1" style="background-color: #CCFF99;">
            <div id="dashBoardTitle_1" class="blue_block" align="left">
                <div id="ord_client_1"></div>
                <div id="pro_name_1"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">現在時間</p>
                        </div>
                        <div align="center">
                            <p id="now_time_1" style="color: #5599FF; font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">訂單數量</p>
                        </div>
                        <div align="center">
                            <p id="ord_qty_1" style="font-size:32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">投入數</p>
                        </div>
                        <div align="center">
                            <p id="input_qty_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">              
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完工數</p>
                        </div>
                        <div align="center">
                            <p id="fin_qty_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default" style="color: #FF0000;">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">尾數</p>
                        </div>
                        <div align="center">
                            <p id="tail_num_1" style="font-size: 32pt; font-weight: bold; color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">開始時間</p>
                        </div>
                        <div align="center">
                            <p id="start_time_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完成時間</p>
                        </div>
                        <div align="center">
                            <p id="fin_time_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計完成時間</p>
                        </div>
                        <div align="center">
                            <p id="exp_fin_time_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計時間差</p>
                        </div>
                        <div align="center">
                            <p id="exp_dif_time_1" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="color: black; font-weight: bold; font-size: 20pt; background-color: #DDFF77;">
                <div id="show_order_1" style="float: left; margin-left: 5%;"></div>
                <div id="show_qty_1" style="float: left; margin-left: 10%;"></div>
                <div id="status_1" style="float: left; margin-left: 10%;"></div>
            </div>
        </div>

        <div class="col-md-4" id="col_2" style="background-color: #CCFF99;">
            <div id="dashBoardTitle_2" class="blue_block" align="left">
                <div id="ord_client_2"></div>
                <div id="pro_name_2"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">現在時間</p>
                        </div>
                        <div align="center">
                            <p id="now_time_2" style="color: #5599FF; font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">訂單數量</p>
                        </div>
                        <div align="center">
                            <p id="ord_qty_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">投入數</p>
                        </div>
                        <div align="center">
                            <p id="input_qty_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完工數</p>
                        </div>
                        <div align="center">
                            <p id="fin_qty_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default" style="color: #FF0000;">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">尾數</p>
                        </div>
                        <div align="center">
                            <p id="tail_num_2" style="font-size: 32pt; font-weight: bold; color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">開始時間</p>
                        </div>
                        <div align="center">
                            <p id="start_time_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完成時間</p>
                        </div>
                        <div align="center">
                            <p id="fin_time_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計完成時間</p>
                        </div>
                        <div align="center">
                            <p id="exp_fin_time_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計時間差</p>
                        </div>
                        <div align="center">
                            <p id="exp_dif_time_2" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="color: black; font-weight: bold; font-size: 20pt; background-color: #DDFF77;">
                <div id="show_order_2" style="float: left; margin-left: 5%;"></div>
                <div id="show_qty_2" style="float: left; margin-left: 10%;"></div>
                <div id="status_2" style="float: left; margin-left: 10%;"></div>
            </div>
        </div>

        <div class="col-md-4" id="col_3" style="background-color: #CCFF99;">
            <div id = "dashBoardTitle_3" class="blue_block" align="left">
                <div id="ord_client_3"></div>
                <div id="pro_name_3"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">現在時間</p>
                        </div>
                        <div align="center">
                            <p id="now_time_3" style="color: #5599FF; font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">訂單數量</p>
                        </div>
                        <div align="center">
                            <p id="ord_qty_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">投入數</p>
                        </div>
                        <div align="center">
                            <p id="input_qty_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完工數</p>
                        </div>
                        <div align="center">
                            <p id="fin_qty_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default" style="color: #FF0000;">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">尾數</p>
                        </div>
                        <div align="center">
                            <p id="tail_num_3" style="font-size: 32pt; font-weight: bold; color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">開始時間</p>
                        </div>
                        <div align="center">
                            <p id="start_time_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">完成時間</p>
                        </div>
                        <div align="center">
                            <p id="fin_time_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計完成時間</p>
                        </div>
                        <div align="center">
                            <p id="exp_fin_time_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div style="background-color: #FFFF33;">
                            <p class="panel-title" align="center">預計時間差</p>
                        </div>
                        <div align="center">
                            <p id="exp_dif_time_3" style="font-size: 32pt; font-weight: bold;"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="color: black; font-weight: bold; font-size: 20pt; background-color: #DDFF77;">
                <div id="show_order_3" style="float: left; margin-left: 5%;"></div>
                <div id="show_qty_3" style="float: left; margin-left: 10%;"></div>
                <div id="status_3" style="float: left; margin-left: 10%;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('myScript')
<script type="text/javascript" src="js/dashboard.js"></script>
@endsection

@section('myStyle')
<link rel="stylesheet" type="text/css" href="css/dashboard.css">
@endsection

@extends('layouts.app')

@section('subPageCss')
  <style type="text/css">
    table {
      table-layout: fixed;
      word-wrap: break-word;
    }
    .mainBtn {
      font-weight: Bold;
      color: black;
    }
    .searchInput {
      width:50px;
    }
    .modal-dialog {
      margin-top: 20vh;
    }
    #errorMsg {
      color: red;
    }
    #updateMsg {
    color: red;
    }
    .set-font {
      font-size: 18px;
    }
  </style>
@endsection

@section('content')
<div class="container" >
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 30px; font-weight: bold;">
                    Antenna 設備參數設定
                </div>

                {{--  <div class="panel-body set-font">
					<div class="form-group row">
                        <form action="{{ route('antenna.index') }}" method="GET">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="search" placeholder="搜尋Antenna名稱" />
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-primary">搜尋</button>
                            </div>
                        </form>
					</div>
				</div>  --}}

                <div class="modal bs-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modalStyle">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<h4 class="modal-title" id="exampleModalLabel" align="center">電子看板編輯</h4>
							</div>
							<div class="modal-body">
								<div class="panel-body">
									<table class="table table-bordered">
										<tr>
                                            <th class="info">序號</th>
                                            <th class="info">電子看板</th>
											<th class="info">類別</th>
											<th class="info">用途說明</th>
                                            {{--  <th class="info">連接 Reader</th>  --}}
                                            <th class="info">設備整合識別碼</th>
										</tr>
                                        <tr>
                                            <td><input type="text" class="form-control" id="edit_id" readonly /></td>
                                            <td><input type="text" class="form-control" id="edit_board" /></td>
                                            <td>
                                                <select id="edit_type" class="form-control">
                                                    <option value="報工">報工</option>
                                                    <option value="排單">排單</option>
                                                    <option value="包裝指示">包裝指示</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" id="edit_note" /></td>
                                            {{--  <td><input type="text" class="form-control" id="edit_reader" /></td>  --}}
                                            <td><input type="text" class="form-control" id="edit_profile" readonly /></td>
                                        </tr>
									</table>
                                    <span id="updateMsg"></span>
								</div>
                                <div class="panel-body">
                                    <button class="btn btn-warning" onclick="updateData()">更新</button>
                                </div>
							</div>
						</div>
					</div>
				</div>

                <div class="panel-body set-font">
                    <table class="table table-striped" id="table">
                        <thead>
                            <tr>
                                <th class="info">序號</th>
                                <th class="info">Antenna 名稱</th>
                                <th class="info">Port 號</th>
                                <th class="info">上次更新時間</th>
                                <th class="info">編輯</th>
                                <th class="info">刪除</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($antennas as $key => $data)
                                <tr>
                                    <td><span>{{ $data->id }}</span></td>
                                    <td><span id="board-{{ $data->id }}">{{ $data->antenna_name }}</span></td>
                                    <td><span id="type-{{ $data->id }}">{{ $data->port_no }}</span></td>
                                    @if (strtotime($data->created_at) !== strtotime($data->updated_at)) 
                                        <td><span>{{ $data->updated_at }}</span></td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                    {{--  <td><button class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="editRow({{ $data }})">編輯</button></td>
                                    <td><button class="btn btn-danger" onclick="deleteData({{ $data->id }})">刪除</button></td>  --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <span id="errorMsg"></span>
                </div>

                <div class="panel-body set-font">
					<div class="form-group row">
						<div class="col-sm-3">
							{{--  <button class="btn btn-primary" onclick="addRow()">新增電子看板</button>  --}}
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script type="text/javascript">
    var tbody = null
    var errMsg = document.getElementById('errorMsg')
    let updateMsg = document.getElementById('updateMsg')
    var rowStatus = 0

    addRow = () => {  
        if (rowStatus === 0) {
            tbody = $('<tbody id="tbody"/>')
            rowStatus = 1
            tbody.append('<tr role="row">'
                +'<td class="active"><button class="btn btn-info" onclick="cancelAddRow()">取消</button></td>'
                +'<td class="active"><input type="text" class="form-control" id="dashboard" placeholder="電子看板名稱" /></td>'
                +'<td class="active"><select id="type" class="form-control">'
                +'<option value="報工">報工</option><option value="排單">排單</option><option value="包裝指示">包裝指示</option></select></td>'
                // +'<td class="active"><input type="text" class="form-control" id="note" placeholder="用途說明 (選填)" /></td>'
                +'<td class="active">'+'<select id="profile" class="form-control">'
                +'<option value="">請選擇線別</option>'
                +'<option value="01100004A001">一群裝配A線</option>'
                +'<option value="01100004B002">一群裝配B線</option>'
                +'<option value="01100004C003">一群裝配C線</option>'
                +'<option value="01100204A004">二群裝配A線</option>'
                +'<option value="01100204B005">二群裝配B線</option>'
                +'<option value="01100204C006">二群裝配C線</option>'
                +'<option value="01100304A007">三群裝配A線</option>'
                +'<option value="01100504B008">五群裝配A線</option>'
                +'<option value="01100504B009">五群裝配B線</option>'+'</select></td>'
                +'<td></td>'
                +'<td class="active"><button class="btn btn-info" onclick="storeData()">確認送出</button></td>'
            )
            
            $('#table').append(tbody)
        }
    }

    cancelAddRow = () => {
        if (rowStatus === 1) {
            rowStatus = 0
            errMsg.innerHTML = ''
            tbody.remove()
        }
    }

    editRow = (data) => {
        document.getElementById('edit_id').value = data.id
        document.getElementById('edit_board').value = data.dashboard_name
        document.getElementById('edit_type').value = data.type
        document.getElementById('edit_note').value = data.note
        // document.getElementById('edit_reader').value = ''
        document.getElementById('edit_profile').value = data.profile
    }

    updateData = () => {
        if (confirm('確定進行更新？')) {
            let id = document.getElementById('edit_id').value
            $.ajax({
                url: '/device/board/update/' + id,
                dataType: 'JSON',
                type: 'PUT',
                data: {
                    dashboard_name: document.getElementById('edit_board').value,
                    type: document.getElementById('edit_type').value,
                    note: document.getElementById('edit_note').value
                },
                success: (response) => {
                    window.location.href = '/device/board'
                },
                statusCode: {
                    422: result => {
                        updateMsg.innerHTML = ''
                        let arr = $.map(JSON.parse(result.responseText), (m, key) => {
                            $('#updateMsg').append('<p>'+m+'</p>')
                        })
                    },
                    404: result => {
                        updateMsg.innerHTML = ''
                        $('#updateMsg').append('<p>查無此資料</p>')
                    }
                }
            })
        }
    }

    deleteData = (id) => {    
        if (confirm('確定刪除資料？')) {  
            $.ajax({
                url: '/device/board/destroy/' + id,
                dataType: 'JSON',
                type: 'DELETE',
                success: (response) => {
                    window.location.href = '/device/board'
                },
                statusCode: {
                    404: result => {
                        errMsg.innerHTML = ''
                        $('#errorMsg').append('<p>查無此資料</p>')
                    }
                }
            })
        }
    }

    storeData = () => {
        let dashboard = document.getElementById('dashboard').value
        let type = document.getElementById('type').value
        let profile = document.getElementById('profile').value
        // let note = document.getElementById('note').value
        if (checkSendData(dashboard, type, profile) === true) {
            $.ajax({
                url: '/device/board/store',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    dashboard_name: dashboard,
                    type: type,
                    // note: note,
                    profile: profile
                },
                success: (response) => {
                    if (response.status === 0) {
                        window.location.href = '/device/board'
                    }
                },
                statusCode: {
                    422: result => {
                        errMsg.innerHTML = ''
                        let arr = $.map(JSON.parse(result.responseText), (m, key) => {
                            $('#errorMsg').append('<p>'+m+'</p>')
                        })
                    }
                }
            })
        }
    }

    checkSendData = (dashboard, type, profile) => {
        if (!dashboard.trim() || !type.trim() || !profile.trim()) {
            alert('資料不允許為空')
        } else {
            return true
        }
    }
</script>
@endsection

@extends('layouts.app')

@section('subPageCss')
 <style>
	table {
  		table-layout: fixed;
  		word-wrap: break-word;
	}
	.mainBtn {
  		font-weight: bold;
  		color: black;
	}
	.searchInput {
  		width: 50px;
	}
	.modal-dialog {
  		margin-top: 20vh;
	}
 </style>
@endsection

@section('content')
<div class="container-fluid" style="width:100%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 30px; font-weight: bold;">
                    錄入模組
                    <button type="button" class="btn btn-default mainBtn" data-toggle="modal" data-target=".bs-example-modal-sm">查詢</button>
                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="form-group">
                                <label for="exampleInputEmail2" style="color: white;">依製令查詢</label>
                                <input type="text" class="form-control" id="moStart">
                                <p style="color: white;">至</p>
                                <input type="text" class="form-control" id="moEnd">
								<button type="submit" class="btn btn-default mainBtn" id="moSubmit">Submit</button><br />

								<label for="exampleInputEmail2" style="color: white;">依報工日期查詢</label>
                                <input type="date" class="form-control" id="dateStart">
                                <p style="color: white;">至</p>
                                <input type="date" class="form-control" id="dateEnd">
                                <button type="submit" class="btn btn-default mainBtn" id="dateSubmit">Submit</button>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-default mainBtn" href="{{ url('/') }}">回首頁</a>
                </div>

                <div class="panel-body">
                  	<table id="tableInputModule_1" class="table table-condensed">
                  		<div style="background-color: #AAFFEE; width: 100%; float: left; font-weight: bold; text-align: center; color: black;">
                            錄入資訊
                        </div>
                      	<tr>
	                      	<td class="active">No</td>
							<td class="active">錄入日期</td>
							<td class="active">錄入ID</td>
							<td class="active">生產製令</td>
							<td class="active">物料名稱</td>
							<td class="active">生產數量</td>
							<td class="active">MacID</td>
							<td class="active">類別</td>
							<td class="active">狀態</td>
							<td class="active">公司別</td>
							<td class="active">廠別</td>
                      	</tr>
                  	</table>
                </div>

                <div class="panel-body">
                  	<table id="tableInputModule_2" class="table table-condensed">
                  		<div style="background-color: #AAFFEE; width: 100%; float: left; font-weight: bold; text-align: center; color: black;">
                            製令資訊
                        </div>
                      	<tr>
							<td class="active">來源訂單號</td>
							<td class="active">客戶</td>
							<td class="active">客戶名稱</td>
                      	</tr>
                  	</table>
                </div>

                <div class="panel-body">
                  	<table id="tableInputModule_3" class="table table-condensed">
                  		<div style="background-color: #AAFFEE; width: 100%; float: left; font-weight: bold; text-align: center; color: black;">
                            讀取設備來源
                        </div>
                      	<tr>
							<td class="active">設備ID</td>
							{{-- <td class="active">Reader ID</td> --}}
							<td class="active">Antenna ID</td>
							<td class="active">Antenna類型</td>
							<td class="active">Port號</td>
							<td class="active">Port ID</td>
							{{-- <td class="active">是否Tag</td>
							<td class="active">是否Start Token</td>
							<td class="active">是否End Token</td> --}}
                      	</tr>
                  	</table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script type="text/javascript">
	var data_1 = $('<tbody id="tbody"/>');
	var data_2 = $('<tbody id="tbody"/>');
	var data_3 = $('<tbody id="tbody"/>');

    $.ajax({
        url: '/api/v1/entryModule/index',
        type: 'GET',
        dataType: 'JSON',
        success: (data) => {
            showData(data);
        }
    });

	$('#moSubmit').click(() => {
		$('.bs-example-modal-sm').modal('hide');
		var moStart = $('#moStart').val();
		var moEnd = $('#moEnd').val();
		$.ajax({
			url: '/api/v1/entryModule/inquire',
			type: 'POST',
			dataType: 'JSON',
			data: {queryType: 'Order', startMoid: moStart, endMoid: moEnd},
			success: (data) => {
				showData(data);
			}
		});
	});

	$('#dateSubmit').click(() => {
		$('.bs-example-modal-sm').modal('hide');
		var dateStart = $('#dateStart').val();
		var dateEnd = $('#dateEnd').val();
		$.ajax({
			url: '/api/v1/entryModule/inquire',
			type: 'POST',
			dataType: 'JSON',
			data: {queryType: 'date', startDate: dateStart, endDate: dateEnd},
			success: (data) => {
				showData(data);
			}
		});
	});

	showData = (data) => {
        $('.info').remove();
        data.map((m, key) => {
            data_1.append('<tr role="row">'
                +'<td class="info">'+m.id+'</td>'
                +'<td class="info">'+m.record_date+'</td>'
                +'<td class="info">'+m.record_id+'</td>'
                +'<td class="info">'+m.mo_id+'</td>'
                +'<td class="info">'+m.item+'</td>'
                +'<td class="info">'+m.mo_qty+'</td>'
                +'<td class="info">'+m.mac_id+'</td>'
                +'<td class="info">'+m.rfid_type+'</td>'
                +'<td class="info">'+m.status_record+'</td>'
                +'<td class="info">'+m.company_id+'</td>'
                +'<td class="info">'+m.factory_id+'</td>'
            );

            data_2.append('<tr role="row">'
                +'<td class="info">'+m.so_id+'</td>'
                +'<td class="info">'+m.customer_id+'</td>'
                +'<td class="info">'+m.customer_name+'</td>'
            );

            data_3.append('<tr role="row">'
                +'<td class="info">'+m.device_id+'</td>'
                +'<td class="info">'+m.antenna_id+'</td>'
                +'<td class="info">'+m.antenna_type+'</td>'
                +'<td class="info">'+m.port_no+'</td>'
                +'<td class="info">'+m.port_id+'</td>'
            );
        })

		$('#tableInputModule_1').append(data_1);
		$('#tableInputModule_2').append(data_2);
		$('#tableInputModule_3').append(data_3);
    }


</script>
@endsection
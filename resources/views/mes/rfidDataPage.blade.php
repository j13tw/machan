@extends('layouts.app')

@section('subPageCss')
 <style>
	table {
  		table-layout: fixed;
  		word-wrap: break-word;
		font-size: 14pt;
	}
	.mainBtn {
  		font-weight: bold;
  		color: black;
	}
	.searchInput {
  		width: 50px;
	}
	.modal-dialog {
  		margin-top: 20vh;
	}

	.modalStyle {
		width: 80%;
	}
 </style>
@endsection

@section('content')
<div class="container-fluid" style="width:100%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 30px; font-weight: bold;">
                    RFID資料數據
                </div>

				<div class="modal bs-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modalStyle">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<h4 class="modal-title" id="exampleModalLabel" align="center">RFID報工紀錄</h4>
							</div>
							<div class="modal-body">
								<div class="panel-body">
									<table id="historyData" class="table table-bordered">
										<tr>
											<th class="info">序號</th>
											<th class="info">報工日期</th>
											<th class="info">來源訂單號</th>
											<th class="info">製令單號</th>
											<th class="info">客戶代碼</th>
											<th class="info">客戶名稱</th>
											<th class="info">物料名稱</th>
											<th class="info">錄入ID</th>
											<th class="info">設備識別碼</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">MacID</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="mac_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">RFID類別</label>
						<div class="col-sm-3">
							<select class="form-control" id="rfid_type">
								<option value="">請選擇RFID類別</option>
								<option value="G">G：開始</option>
								<option value="B">B：一般</option>
								<option value="R">R：完工</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">RFID狀態</label>
						<div class="col-sm-3">
							<select class="form-control" id="rfid_status">
								<option value="">請選擇RFID狀態</option>
								<option value="0">0：已註冊</option>
								<option value="1">1：已錄入</option>
								<option value="2">2：已消除</option>
								<option value="3">3：上線中</option>
								<option value="4">4：已完工</option>
								<option value="5">5：異常處理</option>
								<option value="6">6：失效</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">設備識別碼</label>
						<div class="col-sm-3">
							<select class="form-control" id="profile">
								<option value="">請選擇線別</option>
								<option value="01100004A001">一群裝配A線</option>
								<option value="01100004B002">一群裝配B線</option>
								<option value="01100004C003">一群裝配C線</option>
                                <option value="01100204A004">二群裝配A線</option>
								<option value="01100204B005">二群裝配B線</option>
								<option value="01100204C006">二群裝配C線</option>
							</select>
						</div>
						<input type="submit" class="btn btn-danger" value="搜尋" id="submit" />
					</div>
				</div>

				<div class="panel-body">
					<table id="rfidTable" class="table table-bordered">
						<tr>
							<th class="info">序號</th>
							<th class="info">MacID</th>
							<th class="info">報工日期</th>
							<th class="info">RFID類別</th>
							<th class="info">RFID狀態</th>
							<th class="info">報工紀錄</th>
							<th class="info">來源訂單號</th>
							<th class="info">製令單號</th>
							<th class="info">客戶代碼</th>
							<th class="info">客戶名稱</th>
							<th class="info">物料名稱</th>
						</tr>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script type="text/javascript">
	var datat = $('<tbody id="tbody"/>')
	var historyData = $('<tbody id="tbody"/>')
	
	$('#submit').click(() => {
		var macId = document.getElementById('mac_id').value
		var rfidStatus = document.getElementById('rfid_status').value
		var rfidType = document.getElementById('rfid_type').value
		var profile = document.getElementById('profile').value

		$.ajax({
			'url': '/mes/rfid/data',
			'type': 'POST',
			'dataType': 'JSON',
			'data': {
				mac_id: macId,
				rfid_status: rfidStatus,
				rfid_type: rfidType,
				profile: profile
			},
			success: (data) => {
				showData(data)
			}
		})
	})
	
	showData = (data) => {
        $('.active').remove();
        data.map((m, key) => {
			datat.append('<tr role="row">'
                +'<td class="active">'+m.id+'</td>'
                +'<td class="active">'+m.mac_id+'</td>'
                +'<td class="active">'+m.date+'</td>'
                +'<td class="active">'+m.rfid_type+'</td>'
                +'<td class="active">'+m.rfid_status+'</td>'
                +'<td class="active">'+'<button id="fuck" onclick="show('+"'"+m.mac_id+"'"+')" data-toggle="modal" data-target=".bs-example-modal-sm">查看</button>'+'</td>'
                +'<td class="active">'+m.so_id+'</td>'
                +'<td class="active">'+m.mo_id+'</td>'
                +'<td class="active">'+m.customer_id+'</td>'
                +'<td class="active">'+m.customer_name+'</td>'
                +'<td class="active">'+m.item+'</td>'
            )
        })

		show = (mac_id) => {
			$('.success').remove();
			$.ajax({
				'url': '/mes/rfid/history',
				'type': 'POST',
				'dataType': 'JSON',
				'data': {
					mac_id: mac_id,
				},
				success: (data) => {
					data.map((m, key) => {
						historyData.append('<tr role="row">'
							+'<td class="success">'+m.id+'</td>'
							+'<td class="success">'+m.date+'</td>'
							+'<td class="success">'+m.so_id+'</td>'
							+'<td class="success">'+m.mo_id+'</td>'
							+'<td class="success">'+m.customer_id+'</td>'
							+'<td class="success">'+m.customer_name+'</td>'
							+'<td class="success">'+m.item+'</td>'
							+'<td class="success">'+m.record_id+'</td>'
							+'<td class="success">'+m.start_profile+'</td>'
						);
					})
					$('#historyData').append(historyData)
				}
			})
		}

        $('#rfidTable').append(datat)
    }
	
</script>
@endsection




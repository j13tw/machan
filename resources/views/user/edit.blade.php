@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">使用者資訊修改</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/user/update">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">姓名</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account" class="col-md-4 control-label">帳號</label>

                            <div class="col-md-6">
                                <input id="account" type="text" class="form-control" value="{{ $user->account }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">電子郵件</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="company_id" class="col-md-4 control-label">公司別</label>

                            <div class="col-md-6">
                                <select id="company_id" name="company_id" class="form-control">
                                    <option value="" selected></option>
                                    <option value="01" {{ $user->company_id === '01' ? 'selected' : '' }}>01</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="line_id" class="col-md-4 control-label">線別</label>

                            <div class="col-md-6">
                                <select id="line_id" name="line_id" class="form-control">
                                    <option value="" selected></option>
                                    <option value="A" {{ $user->line_id === 'A' ? 'selected' : '' }}>A</option>
                                    <option value="B" {{ $user->line_id === 'B' ? 'selected' : '' }}>B</option>
                                    <option value="C" {{ $user->line_id === 'C' ? 'selected' : '' }}>C</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="org_id" class="col-md-4 control-label">組織別</label>

                            <div class="col-md-6">
                                <select id="org_id" name="org_id" class="form-control">
                                    <option value="" selected></option>
                                    <option value="1000" {{ $user->org_id === '1000' ? 'selected' : '' }}>1000</option>
                                    <option value="1002" {{ $user->org_id === '1002' ? 'selected' : '' }}>1002</option>
                                    <option value="1003" {{ $user->org_id === '1003' ? 'selected' : '' }}>1003</option>
                                    <option value="1005" {{ $user->org_id === '1005' ? 'selected' : '' }}>1005</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role_id" class="col-md-4 control-label">角色</label>

                            <div class="col-md-6">
                                <select id="role_id" name="role_id" class="form-control">
                                    <option value="" selected></option>
                                    <option value="QC" {{ $user->role_id === 'QC' ? 'selected' : '' }}>QC</option>
                                    <option value="PM" {{ $user->role_id === 'PM' ? 'selected' : '' }}>PM</option>
                                    <option value="MFG" {{ $user->role_id === 'MFG' ? 'selected' : '' }}>MFG</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="routing" class="col-md-4 control-label">製程</label>

                            <div class="col-md-6">
                                <select id="routing" name="routing" class="form-control">
                                    <option value="" selected></option>
                                    <option value="04" {{ $user->routing === '04' ? 'selected' : '' }}>04</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="確認送出" />
                                <input type="submit" class="btn btn-danger" value="取消返回" onclick="window.location.href='/'" />
                            </div>
                            
                        </div>
                        
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

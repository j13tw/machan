<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');                 //訊息碼
            $table->string('notification_id');      //通知訊息代碼
            $table->string('message');              //通知訊息
            $table->string('title');                //通知主旨
            $table->string('content');              //通知內容
            $table->string('method');               //發送方式
            $table->string('timing');               //發送時機
            $table->string('cycle');                //週期
            $table->string('assign_type');          //接收指定類型
            $table->string('name');                 //人員指定 (可多選)
            $table->string('role_name');            //角色指定
            $table->string('line_id');
            $table->string('sender');               //發動通知系統
            $table->string('receiver');             //承接通知系統
            $table->string('ref');                  //關聯APP
            $table->string('next_action');           //下一步系統作業
            $table->string('ntf_table');            //通知資料表
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_notifications');
    }
}

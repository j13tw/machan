<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbMosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb_mos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dashboard_id');
            $table->string('dashboard_name');
            $table->integer('status');
            $table->integer('eb_qty');
            $table->integer('line_mo_qty');
            $table->integer('rank');
            $table->string('type');
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->date('date')->nullable();
            $table->string('mo_id');
            $table->string('item');
            $table->integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('mo_status');
            $table->integer('accumulator_start');
            $table->integer('accumulator_fin');
            $table->integer('ng');
            $table->time('mo_start_time')->nullable();
            $table->date('mo_finish_date')->nullable();
            $table->time('mo_finish_time')->nullable();
            $table->integer('finish');
            $table->time('start_time')->nullable();
            $table->date('finish_date')->nullable();
            $table->time('finish_time')->nullable();
            $table->string('rest_time');
            $table->integer('prod_qty');
            $table->integer('fin_qty');
            $table->integer('nf_qty');
            $table->string('change_lead_time');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('nex_id');
            $table->string('suspend_time');
            $table->string('first_pass_rate');
            $table->integer('acture_human');
            $table->string('tht');
            $table->string('ht');
            $table->string('tct');
            $table->string('ct');
            $table->string('qty_per_hr');
            $table->string('productivity');
            $table->string('reach_rate');
            $table->string('f_completion_time');
            $table->integer('target_qty');
            $table->string('suspend_rate');
            $table->string('std_change_lead');
            $table->string('equivalents');
            $table->string('immediate_tct');
            $table->string('immediate_ct');
            $table->string('immediate_qty_per_hr');
            $table->string('immediate_productivity');
            $table->string('capacity_rate');
            $table->string('color');
            $table->string('immediate_equivalents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb_mos');
    }
}

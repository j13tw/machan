<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupReadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_readers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reader_id');
            $table->string('reader_name');
            $table->string('mac_id');
            $table->string('reader_ip');
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_readers');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoSevensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_sevens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->boolean('separate');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->string('start_line');
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->date('finish_date')->nullable();
            $table->time('finish_time')->nullable();
            $table->string('rest_time');
            $table->integer('accumulator_start');
            $table->integer('accumulator_fin');
            $table->boolean('finish');
            $table->string('change_time');
            $table->integer('ng_qty');
            $table->string('ng_reason');
            $table->time('ng_start_time')->nullable();
            $table->time('ng_finish_time')->nullable();
            $table->string('suspend_reason');
            $table->string('exp_change_time');
            $table->string('suspend');
            $table->integer('acture_human');
            $table->double('target_tht');
            $table->double('target_ht');
            $table->double('bottle_ct');
            $table->double('ht');
            $table->double('first_pass_rate');
            $table->string('entry_register');   // 入庫申請單 (用途未知)
            $table->double('cross_sum_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_sevens');
    }
}

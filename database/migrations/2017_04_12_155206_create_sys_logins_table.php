<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_logins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sys_id');               //代碼
            $table->string('name');                 //名稱
            $table->string('type');                 //類別 (公司 工廠 工作中心)
            $table->string('company_id');           //公司別
            $table->string('org_id');               //廠別
            $table->string('group');                //隸屬別
            $table->string('short_name');           //簡稱
            $table->Integer('active')->default(1);  //有效
            $table->string('tel');                  //電話
            $table->string('address');              //地址
            $table->string('note');                 //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_logins');
    }
}

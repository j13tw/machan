<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('line_id');              
            $table->string('line_name');            
            $table->string('company_id');           
            $table->string('org_id');               
            $table->string('routing');              
            $table->string('type');                 
            $table->Integer('human');               
            $table->string('profile');              
            $table->Integer('status')->default(1);  
            $table->integer('first_condition')->default(90);   
            $table->integer('second_condition')->default(60);  
            $table->string('note');                 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_lines');
    }
}

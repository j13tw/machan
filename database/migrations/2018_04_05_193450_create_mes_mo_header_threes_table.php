<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderThreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_threes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->string('mixed_mo_id');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->date('mix_start_date')->nullable();
            $table->time('mix_start_time')->nullable();
            $table->date('mix_finish_date')->nullable();
            $table->time('mix_finish_time')->nullable();
            $table->integer('accu_insert_finish');
            $table->string('mix_rest_time');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('insert_change_time');
            $table->double('mix_sum_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_threes');
    }
}

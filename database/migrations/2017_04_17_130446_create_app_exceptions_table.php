<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_exceptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('notification_id');
            $table->string('message');
            $table->string('title');
            $table->string('content');
            $table->date('infor_date')->nullable();
            $table->time('infor_time')->nullable();
            $table->string('name');
            $table->string('role_name');
            $table->string('mac_id');
            $table->integer('rfid_type');
            $table->string('record_id');
            $table->string('company_id');
            $table->string('org_id');
            $table->string('rfid_status');
            $table->date('date')->nullable();
            $table->string('mo_id');
            $table->string('item');
            $table->integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('mo_status');
            $table->string('line_id');
            $table->string('line_name');
            $table->string('routing');
            $table->string('person_id');
            $table->string('role_id');
            $table->string('tablet_id');
            $table->integer('nf_type');
            $table->string('ex_id');
            $table->string('ex_code');
            $table->integer('exception');
            $table->string('except_reason');
            $table->time('ex_start_time')->nullable();
            $table->time('ex_end_time')->nullable();
            $table->integer('reply');
            $table->date('reply_date')->nullable();
            $table->time('reply_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_exceptions');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoTensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_tens', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('sep_datetime')->nullable();
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->string('person');
            $table->string('item');
            $table->integer('qty');
            $table->string('completion_time');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_tens');
    }
}

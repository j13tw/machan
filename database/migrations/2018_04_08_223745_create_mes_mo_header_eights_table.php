<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderEightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_eights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->string('rest_time');
            $table->string('accu_change_time');
            $table->integer('acture_human');
            $table->double('target_tht');
            $table->double('target_ht');
            $table->double('sum_tct');
            $table->double('sum_tht');
            $table->double('first_pass_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_eights');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupPrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_prints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('b_print_id');               //藍芽印表機ID
            $table->string('b_print_name');             //藍芽印表機名稱
            $table->string('tablet_id');                //設備 ID
            $table->string('reader_id');                //Reader ID
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_prints');
    }
}

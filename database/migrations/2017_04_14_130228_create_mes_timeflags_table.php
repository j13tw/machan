<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesTimeflagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_timeflags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile');          //設備整合識別碼
            $table->integer('type');            //0開線 1開始 2完工 3下班
            $table->date('date')->nullable();  //日期( G tag 過Start，type = 1)
            $table->time('time')->nullable();               //時間
            $table->time('cl_end_time')->nullable();        //換模結束時間 (製令開始時間)
            $table->time('cl_start_time')->nullable();      //換模開始時間 (R tag過Start)
            $table->string('mo_id');            //製令單號
            $table->string('change_time');      //換線時間
            $table->string('note');             //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_timeflags');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesRfidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_rfids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mac_id');               // RFID MAC
            $table->string('record_id');            // 錄入 ID
            $table->string('rfid_type');            // 類別
            $table->integer('rfid_status');         // RFID 狀態
            $table->string('mo_id');                // 製令單號
            $table->string('item');                 // 物料名稱 (WS T9)
            $table->string('so_id');                // 來源訂單號 (WS T9)
            $table->string('customer_id');          // 客戶ID (WS T9)
            $table->string('customer_name');        // 客戶名稱 (WS T9)
            $table->string('start_line');           // 開始線別(A B C)
            $table->string('start_profile');        // 開始設備識別碼
            $table->string('start_antenna_id');     // 開始讀取設備號
            $table->dateTime('start_datetime')->nullable();     // 開始日期/時間
            $table->string('end_line');             // 完工線別
            $table->string('end_profile');          // 完工設備識別碼
            $table->string('end_antenna_id');       // 完工讀取設備號
            $table->dateTime('end_datetime')->nullable();       // 完工日期/時間
            $table->string('qc_pass');              // QC PASS 序號
            $table->string('qc');                   // 品保員
            $table->integer('finish');              // 製令完工識別
            $table->time('finish_time')->nullable();            // 製令完工時間
            $table->integer('ng');                  // 尾數
            $table->string('exception_reason');     // 異常原因
            $table->time('ex_start_time')->nullable();          // 異常開始時間
            $table->time('ex_end_time')->nullable();            // 異常結束時間
            $table->integer('suspend');             // 除外工時
            $table->string('notification_id');      // 通知訊息代碼
            $table->dateTime('ntf_date_time')->nullable();      // 通知日期
            $table->string('notification');         // 通知紀錄
            $table->string('remark');               // 備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_rfids');
    }
}

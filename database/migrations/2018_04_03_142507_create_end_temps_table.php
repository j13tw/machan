<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('end_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfid_type');
            $table->string('tag_id');
            $table->string('mac_id');
            $table->integer('rfid_status');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->dateTime('start_datetime')->nullable();
            $table->string('end_line');
            $table->string('end_profile');
            $table->string('end_antenna_id');
            $table->dateTime('end_datetime')->nullable();
            $table->integer('unfinish_qty');
            $table->boolean('last');
            $table->boolean('current_finish');
            $table->boolean('ng');
            $table->boolean('mo_finish');
            $table->string('continuous');
            $table->string('continuous_param');
            $table->boolean('finish_insert');
            $table->boolean('mix_order');
            $table->boolean('cross_line');
            $table->boolean('except');
            $table->string('real_rest_time');
            $table->string('rest_mo_rest_time');
            $table->integer('acture_human');
            $table->double('qty_hour');
            $table->double('manufacture');
            $table->double('sum_real_tct');
            $table->double('sum_real_tht');
            $table->double('real_tht');
            $table->double('real_ht');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('end_temps');
    }
}

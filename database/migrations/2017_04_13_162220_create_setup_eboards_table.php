<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupEboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_eboards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dashboard_id');             //電子看板ID
            $table->string('dashboard_name');           //電子看板名稱
            $table->string('type');                     //類別(報工 排單 包裝指示)
            $table->string('purpose');                  //用途說明
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_eboards');
    }
}

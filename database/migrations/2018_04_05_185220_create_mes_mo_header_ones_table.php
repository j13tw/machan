<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_ones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->date('current_start_date')->nullable();
            $table->time('current_start_time')->nullable();
            $table->date('current_finish_date')->nullable();
            $table->time('current_finish_time')->nullable();
            $table->integer('prod_qty');
            $table->integer('fin_qty');
            $table->integer('ng_qty');
            $table->string('current_rest_time');
            $table->string('off_work_rest_time');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('change_time');
            $table->double('tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_ones');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbTriggerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb_trigger_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tg_date');                  //日期
            $table->string('tg_time');                  //時間
            $table->string('trigger_id');
            $table->string('trigger_name');
            $table->string('dashboard_id');
            $table->string('dashboard_name');
            $table->string('type');
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->Integer('status');
            $table->string('mac_id');
            $table->string('rfid_type');
            $table->string('record_id');
            $table->string('company_id');
            $table->Integer('rfid_status');
            $table->Integer('priority');
            $table->Integer('refresh_trigger');
            $table->Integer('close');
            $table->Integer('close_trigger');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb_trigger_logs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_records', function (Blueprint $table) {
            $table->increments('id');
            $table->date('record_date')->nullable();              // 錄入日期
            $table->string('record_id');                // 錄入 ID
            $table->string('complete_date');            // 完成日期
            $table->string('mo_id');                    // 生產製令
            $table->string('item');                     // 物料名稱
            $table->Integer('mo_qty');                  // 生產數量
            $table->string('mac_id');                   // RFID MAC
            $table->integer('group_id');                // 判定是否拆單 (拆1次 則+1) 
            $table->integer('group_qty');               // 拆單數量
            $table->string('group_line');               // 預設線別 (以G為主)
            $table->string('rfid_type');                // 類別
            $table->Integer('rfid_status');             // 狀態
            $table->string('company_id');               // 公司別
            $table->string('factory_id');               // 廠別
            $table->string('so_id');                    // 來源訂單號
            $table->string('customer_id');              // 客戶 ID
            $table->string('customer_name');            // 客戶 NAME
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_records');
    }
}

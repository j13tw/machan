<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('record_date')->nullable();    // 錄入日期
            $table->string('record_id');                // 錄入 ID
            $table->integer('group_id');                // 判定是否拆單 (拆1次 則+1) 
            $table->integer('group_qty');               // 拆單數量
            $table->string('group_line');               // 預設線別 (以G為主)
            $table->string('mo_id');                    // 製令單號
            $table->string('serial_no');                // 生產序號
            $table->integer('accumulator_start');       // 累計開工數
            $table->integer('accumulator_fin');         // 累計完工數
            $table->date('change_date')->nullable();    // 換線日期
            $table->string('change_time');              // 換線時間
            $table->string('change_line_id');           // 換線別ID
            $table->string('change_line_name');         // 換線別名稱
            $table->string('change_profile');           // 設備整合識別碼
            $table->integer('mo_status');               // 狀態
            $table->string('item');                     // 物料名稱
            $table->integer('qty');                     // 生產數量
            $table->string('completion_date');          // 需求完工日期
            $table->string('so_id');                    // 來源訂單號
            $table->string('customer_id');              // 客戶 ID
            $table->string('customer_name');            // 客戶名稱
            $table->date('date')->nullable();           // 報工日期
            $table->string('line_id');
            $table->string('line_name');
            $table->string('profile');
            $table->time('start_time')->nullable();     // 製令開始時間
            $table->date('finish_date')->nullable();    // 製令完工日期
            $table->time('finish_time')->nullable();    // 製令完工時間
            $table->integer('finish');                  // 製令完工識別
            $table->time('first_start_time')->nullable();           // 當批開始時間
            $table->date('first_finish_date')->nullable();          // 當批完工日期
            $table->time('first_finish_time')->nullable();          // 當批完工時間
            $table->string('rest_time');                // 當批休息時間
            $table->integer('prod_qty');                // 當批投入數
            $table->integer('fin_qty');                 // 當批完工數
            $table->integer('nf_qty');                  // 當批尾數
            $table->string('change_lead_time');         // 換模換線時間
            $table->time('cl_start_time')->nullable();  // 換模開始時間
            $table->time('cl_end_time')->nullable();    // 換模結束時間
            $table->string('nex_id');                   // 除外 ID
            $table->string('suspend_time');             // 除外工時
            $table->string('first_pass_rate');          // 一次通過率
            $table->integer('acture_human');            // 實際直接人數
            $table->string('tht');                      // 來自ANPS產品數據
            $table->string('ht');                       // THT / 實際直接人數
            $table->string('tct');
            $table->string('ct');
            $table->integer('qty_per_hr');              // 每小時數量
            $table->string('productivity');             // 勞動生產力
            $table->string('reach_rate');               // 達成率
            $table->string('f_completion_time');        // 預計完成時間
            $table->string('target_qty');               // 目標生產數量
            $table->string('suspend_rate');             // 除外工時比
            $table->string('std_change_lead');          // 標準換模換線時間
            $table->string('equivalents');              // 約當產量
            $table->string('immediate_tct');            // 即時 TCT
            $table->string('immediate_ct');             // 即時 CT
            $table->integer('immediate_qty_per_hr');    // 目前每小時數量
            $table->string('immediate_productivity');   // 目前勞動生產力
            $table->string('capacity_rate');            // 產能達成率
            $table->string('color');                    // G產能90%↑ Y60~90% R60%↓
            $table->string('immediate_equivalents');    // 即時約當產量
            $table->integer('code');                    // 通知紀錄欄位(最新一筆)
            $table->string('notification_id');          // 通知訊息代碼
            $table->dateTime('ntf_date_time')->nullable();          // 發送時間
            $table->string('notification');             // 通知紀錄(可多筆)
            $table->string('remark');                   // 備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderFoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_fours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->double('delta_t');
            $table->date('ng_start_date')->nullable();
            $table->time('ng_start_time')->nullable();
            $table->date('ng_finish_date')->nullable();
            $table->time('ng_finish_time')->nullable();
            $table->integer('accu_ng_fin_qty');
            $table->double('ng_delta_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_fours');
    }
}

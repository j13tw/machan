<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account');          //帳號 (員工工號)
            $table->string('password');         //密碼
            $table->Integer('sync');            //同步T9 0為否 1為是
            $table->string('employee_id');      //員工工號
            $table->string('name');             //員工名稱
            $table->string('company_id');       //公司別
            $table->string('org_id');           //廠別
            $table->string('routing');          //製程類別
            $table->string('role_id');          //角色 ID
            $table->string('role_name');        //角色名稱
            $table->Integer('active');          //有效
            $table->string('note');             //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_accounts');
    }
}

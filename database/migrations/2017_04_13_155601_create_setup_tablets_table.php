<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupTabletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_tablets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tablet_id');                //tablet511
            $table->string('mac_id');                   //tablet511 macID
            $table->string('role');                     //角色
            $table->string('ip');                       //tablet ip
            $table->string('line_id');                  //線別 ID
            $table->string('org_id');                   //廠別
            $table->string('routing');                  //製程類別
            $table->string('profile');                  //設備整合識別碼
            $table->string('antenna_id');               //天線 ID
            $table->string('note');                     //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_tablets');
    }
}

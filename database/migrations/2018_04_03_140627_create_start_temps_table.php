<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfid_type');
            $table->string('tag_id');
            $table->string('mac_id');
            $table->integer('rfid_status');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->dateTime('start_datetime')->nullable();
            $table->boolean('first');
            $table->string('continuous');
            $table->string('continuous_param');
            $table->boolean('judge_insert');
            $table->boolean('cross_line');
            $table->boolean('sep_order');
            $table->boolean('except');
            $table->string('day_rank');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('change_time');
            $table->boolean('except_notify');
            $table->string('s_change_time');
            $table->string('insert_change_time');
            $table->string('sep_change_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_temps');
    }
}

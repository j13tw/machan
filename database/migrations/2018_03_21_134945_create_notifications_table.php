<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('notification_id');
            $table->string('message');
            $table->string('title');
            $table->string('content');
            $table->date('infor_date')->nullable();
            $table->time('infor_time')->nullable();
            $table->string('name');
            $table->string('role_name');
            $table->Integer('reply');
            $table->date('reply_date')->nullable();
            $table->time('reply_time')->nullable();
            $table->string('sender');
            $table->string('receiver');
            $table->string('mac_id');
            $table->string('rfid_type');
            $table->string('company_id');
            $table->string('factory_id');
            $table->Integer('rfid_status');
            $table->string('mo_id');
            $table->string('item');
            $table->Integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->string('org_id');
            $table->Integer('mo_status');
            $table->string('line_id');
            $table->string('line_name');
            $table->string('routing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
	Route::group(['prefix' => 'user'], function () {
		Route::middleware('jwt.auth')->get('/', 'api\UserController@show');						// 取得登入者資訊
		Route::post('auth', 'api\AuthController@auth');											// 登入
		Route::post('register', 'api\AuthController@register');									// 註冊
	});

	Route::post('pass/manufacture/start', 'api\RfidController@passRfidStart');					// RFID通過start
	
	Route::middleware('jwt.auth')->group(function () {
		Route::get('check/mapping/mo/{mo_id}', 'api\ProduceOrderController@checkMappingMoId');	// 檢查生產製令
		Route::get('record/tag/{mo_id}', 'api\RfidController@queryTag');						// 查詢製令錄入數量
		Route::post('register/rfid', 'api\RfidController@registerRFID');						// RFID註冊
		Route::post('recording/rfid', 'api\RfidController@recordingRFID');						// RFID錄入製令
		Route::put('erase/rfid/tag', 'api\RfidController@eraseRfidTag');						// RFID消除製令
		Route::post('pass/manufacture/end', 'api\RfidController@passRfidEnd');					// RFID通過end
		Route::post('print/qcpass/state', 'api\RfidController@createPrintState');				// 列印QC PASS
		Route::put('qcpass/batch/reprint', 'api\RfidController@batReprint');					// 批次重複列印
		Route::put('{mo_id}/force/closing', 'api\RfidController@forceClosing');					// 強制結案
		Route::put('advance/qcpass/print', 'api\RfidController@advancePrint');					// 預先列印
		Route::put('force/qcpass/print', 'api\RfidController@forcePrint');						// 強制列印
		Route::post('check/rfid/status', 'api\RfidController@checkRfidTag');					// 檢查RFID狀態

		Route::get('get/rfid/info/{mac_id}', 'api\InquireController@getRfidInfo');				// RFID歷史紀錄  add ng column
		Route::get('get/online/mo', 'api\InquireController@getOnlineMo');						// 取得線上製令
		Route::get('get/erase/mo/{mo_id}', 'api\InquireController@getEraseMo'); 				// 取得欲消除之製令資訊
		Route::get('get/mo/{mo_id}/info', 'api\InquireController@getMoInfo');					// 製令報工數據 明細
		Route::get('get/{mo_id}/advance/print', 'api\InquireController@getQcpassAdvancePrint');	// 預先列印明細
		Route::get('get/qcpass/force/print/list', 'api\InquireController@getQcpassForceList');	// 強制列印清單
		Route::get('get/{mo_id}/force/print/info', 'api\InquireController@getQcpassForceInfo');	// 強制列印明細
		Route::post('get/mo/list', 'api\InquireController@getMoList');							// 製令報工數據 清單
		Route::post('get/mo/qcpass/info', 'api\InquireController@getQcpassInfo');				// QC PASS 模組 (Android) 

		Route::group(['prefix' => 'unfinished'], function () {
			Route::get('index', 'api\InquireController@reasonIndex');							// 取得所有尾數異常原因
			Route::post('store', 'api\InquireController@createReason');							// 新增尾數異常原因
		});

		Route::get('exception/index', 'api\InquireController@exceptionIndex');					// 取得所有除外工時原因

		// 上下班按鈕 未來改為判斷登入者 並取消平板綁定
		Route::group(['prefix' => 'reader'], function () {
			Route::post('off', 'api\DeviceController@workOff');									// 下班
			Route::post('on', 'api\DeviceController@workOn');									// 上班
			Route::post('restart', 'api\DeviceController@restartReader');						// 重啟Reader
		});

		// 績效看板自定義變色條件
		Route::group(['prefix' => 'board'], function () {
			Route::put('{profile}/change/color', 'api\DashboardController@changeCondition');	// 變更看板變色條件
			Route::get('profile', 'api\DashboardController@profileIndex');							// 顯示所有設備整合識別碼 (供上下班按鈕使用)
		});
	
		// 通知
		Route::group(['prefix' => 'notify'], function () {
			Route::get('info', 'api\NotifyController@index');									// 通知總表
			Route::put('input/people/{id}', 'api\NotifyController@inputPeople');				// 輸入開線人數
			Route::put('input/mo/start/{id}', 'api\NotifyController@inputMoStart');				// 輸入製令開始時間
			Route::put('input/product/start/{id}', 'api\NotifyController@inputProStart');		// 輸入產品開始時間
			Route::put('input/unfinished/{id}', 'api\NotifyController@inputUnfinished');		// 輸入尾數異常原因
			Route::put('input/suspend/{id}', 'api\NotifyController@inputSuspend');				// 輸入除外工時 (換魔換線、作業效率異常)
			Route::put('reply/{id}', 'api\NotifyController@reply');								// 回覆通知

			// 通知列表
			Route::group(['prefix' => 'list'], function () {
				Route::get('{code}', 'api\NotifyListController@index');							// 通知列表
			});
		});
	});
});

<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Requests\User\Auth;
use App\Http\Requests\User\Register;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use JWTAuth;

class AuthController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function auth(Auth $request)
    {
        $credentials = request()->only('account', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 1, 'message' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 1, 'message' => 'could not create token'], 500);
        }

        return response()->json(['status' => 0, 'token' => $token]);
    }

    public function register(Register $request)
    {
        $user = $this->userRepo->create(request()->all());
        if (!$user) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }
}

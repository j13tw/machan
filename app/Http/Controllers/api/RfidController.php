<?php

namespace App\Http\Controllers\api;

use Log;
use Redis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Rfid\Erase;
use App\Http\Requests\Rfid\Record;
use App\Http\Requests\Rfid\Register;
use App\Http\Requests\Rfid\PassEnd;
use App\Http\Requests\Rfid\PassStart;
use App\Http\Requests\Rfid\BatReprint;
use App\Http\Requests\Rfid\ForcePrint;
use App\Http\Requests\Rfid\AdvancePrint;
use App\Http\Requests\Rfid\ForceClosing;
use App\Http\Requests\Rfid\CreateQcPass;
use App\Repositories\RfidRepository;
use App\Services\GetProduceOrder;
use App\Services\CalculateWorkHours;
use App\Services\NodeReaderService;
use App\Events\RfidStart;
use App\Events\RfidEnd;
use App\Events\QcPass;

class RfidController extends Controller
{
    private $iotService;
    private $produceService;
    protected $rfidRepo;

    public function __construct(RfidRepository $rfidRepo, GetProduceOrder $produceService, NodeReaderService $iotService)
    {
        $this->rfidRepo = $rfidRepo;
        $this->produceService = $produceService;
        $this->iotService = $iotService;
    }

    /**
     * RFID 註冊
     */
    public function registerRFID(Register $request)
    {
        $data = json_encode(request()->all());
        Redis::publish('register-channel', $data);
    	return response()->json(['status' => 0, 'message' => 'register success']);
    }
    
    /**
     * RFID 錄入製令
     */
    public function recordingRFID(Record $request)
    {
        $params = json_encode([['TypeId' => 'MO10', 'BillNo' => request()->mo_id]]);
        $produceInfo = $this->produceService->produceInfo($params);
        $checkRfid = $this->rfidRepo->mappingRfid(request()->all());
        $rfidStatus = $this->rfidRepo->checkRfidStatus(request()->mac_id);
        if ($produceInfo === 1) {
            return response()->json(['status' => 1, 'message' => 'web service connect error']);
        }
        if ($checkRfid === 1) {
            return response()->json(['status' => 1, 'message' => 'The RFID has been recorded']);
        }
        if (!empty($rfidStatus)) {
            return response()->json([
                'status' => 1,
                'message' => 'RFID is not completed',
                'data' => $rfidStatus
            ]);
        }
        $data = $checkRfid;
        $tht = $this->produceService->getTht($produceInfo['item']);
        if (!$tht) {    // 判斷是否取得tht
            return response()->json(['status' => 1, 'message' => 'do not get tht from webservice']);
        }
        $data = array_merge($data, $produceInfo, $tht);
        Redis::publish('mapping-channel', json_encode($data));
        return response()->json([
            'status' => 0,
            'message' => 'mapping success',
            'start' => $checkRfid['start'],
            'end' => $checkRfid['end'],
        ]);
    }

    /**
     * 查詢該製令已錄入數量
     */
    public function queryTag($mo_id)
    {
        $data = $this->rfidRepo->getTagQty($mo_id);
        if (count($data) === 0) {
            return response()->json(['status' => 1, 'message' => 'mo_id not found']);
        }
        return response()->json([
            'status' => 0,
            'message' => 'query successfully',
            'data' => $data
        ]);
    }

    /**
     * RFID 通過 Start Reader
     */
    public function passRfidStart(PassStart $request)
    {
        $data = $this->rfidRepo->rfidStart(request()->all());
        if ($data === 1) {
            event(new RfidStart(request()->mac_id));   // 觸發開始區事件
            return response()->json(['status' => 1, 'message' => 'The RFID tag has been used']);
        }
        $data = json_encode($data);
        Redis::publish('start-channel', $data); // Redis：通過綠色 寫入通知資料表
        Log::info(request()->mac_id.' has been published to start-channel');
        return response()->json([
            'status' => 0,
            'message' => 'pass start success',
            'rfid_type' => json_decode($data)->rfid_type    // 回傳RFID顏色
        ]);
    }

    /**
     * RFID 通過 End Reader
     */
    public function passRfidEnd(PassEnd $request)
    {
        // 完工無開始
        $user = auth()->user();
        $checkStart = $this->rfidRepo->checkRfidStart($user, request()->all());
        if ($checkStart !== 1) {
            Redis::publish('start-channel', json_encode($checkStart));
            Log::info(request()->mac_id.' has been published to start-channel (non-start)');
        }

        $data = $this->rfidRepo->rfidEnd($user, request()->all());
        if ($data === 1) {
            event(new RfidEnd(request()->all()));   // 觸發完工區事件
            return response()->json(['status' => 1, 'message' => 'The RFID has not been erased']);
        }
        if ($data === 2) {
            event(new RfidEnd(request()->all()));
            return response()->json(['status' => 1, 'message' => 'The RFID has not been recorded']);
        }
        if ($data === 3) {
            event(new RfidEnd(request()->all()));
            return response()->json(['status' => 1, 'message' => 'The RFID has been finished']);
        }
        $end_datetime = $this->rfidRepo->getDateTime(request()->mac_id, 'B', $data);
        $mo_datetime = $this->rfidRepo->getDateTime(request()->mac_id, 'R', $data);
        $item_name = $this->produceService->getBillNoName($data['item']);
        $data = json_encode($data);
        Redis::publish('end-channel', $data);
        Log::info(request()->mac_id.' has been published to end-channel');
        return response()->json([
            'status' => 0,
            'message' => 'pass end success',
            'qc_pass' => json_decode($data)->qc_pass,
            'rfid_type' => json_decode($data)->rfid_type,
            'customer_name' => json_decode($data)->customer_name,
            'item' => json_decode($data)->item,
            'item_name' => $item_name ? $item_name->MaterialName : '(無法取得料名)',
            'mo_id' => json_decode($data)->mo_id,
            'rfid_status' => json_decode($data)->rfid_status,
            'mac_id' => request()->mac_id,
            'end_datetime' => $end_datetime,
            'mo_datetime'=> $mo_datetime
        ]);
    }

    /**
     * 列印 QC PASS (0:一般列印  1:重複列印)
     */
    public function createPrintState(CreateQcPass $request)
    {
        $user = auth()->user();
        $qcpassInfo = $this->rfidRepo->printQcPass($user, request()->all());
        if ($qcpassInfo === 1) {
            return response()->json(['status' => 1, 'message' => 'The qc pass data error']);
        }
        if ($qcpassInfo === 2) {
            return response()->json(['status' => 1, 'message' => 'The mac_id has been printed']);
        }
        if ($qcpassInfo['mo_status'] === 1) {
            $immeTct = $this->iotService->getTct($qcpassInfo['mo_id']);
        }
        Redis::publish('qcpass-channel', json_encode($qcpassInfo));
        Log::info(request()->qc_pass.' has been published to qcpass-channel');
        return response()->json(['status' => 0, 'message' => 'print success']);
    }

    /**
     * 批次重複列印
     */
    public function batReprint(BatReprint $request)
    {
        $this->rfidRepo->reprint(request()->all());
        event(new QcPass(request()->all()));
        return response()->json(['status' => 0, 'message' => 'reprint successfully']);
    }

    /**
     * 預先列印
     */
    public function advancePrint(AdvancePrint $request)
    {
        $user = auth()->user();
        $advanceInfo = $this->rfidRepo->advancePrint($user, request()->all());
        if ($advanceInfo === false) {
            return response()->json(['status' => 1, 'message' => 'The qc pass data error']);
        }
        Redis::publish('advance-channel', json_encode($advanceInfo));
        return response()->json(['status' => 0, 'message' => 'print success']);
    }

    /**
     * 強制列印
     */
    public function forcePrint(ForcePrint $request)
    {
        $user = auth()->user();
        $forceInfo = [
            'mo_id' => request()->mo_id,
            'state' => 3,
            'line_id' => $user->line_id,
            'profile' => $this->rfidRepo->getProfile($user)->profile,
            'qc' => $user->name,
        ];
        Redis::publish('advance-channel', json_encode($forceInfo));
        return response()->json(['status' => 0, 'message' => 'print success']);
    }

    /**
     * RFID 消除
     */
    public function eraseRfidTag(Erase $request)
    {
        $rfids = json_encode(request()->all());
        Redis::publish('erase-channel', $rfids);
        return response()->json(['status' => 0, 'message' => 'Erase success']);
    }
    
    /**
     * 回傳 RFID 狀態
     */
    public function checkRfidTag()
    {
        $rfidInfo = $this->rfidRepo->rfidTagStatus(request()->mac_id);
        return response()->json([
            'status' => 0,
            'message' => 'success',
            'data' => $rfidInfo
        ]);
    }

    /**
     * 強制結案
     */
    public function forceClosing($mo_id)
    {
        $moInfo = $this->rfidRepo->forceClosing($mo_id);
        if ($moInfo === false) {
            return response()->json(['status' => 1, 'message' => 'The mo_id has been finished']);
        }
        Redis::publish('end-channel', json_encode($moInfo));   // 強制結案 以end-channel處理
        return response()->json(['status' => 0, 'message' => 'Close Successfully']);
    }


    // public function computeWork()
    // {
    //     $start = strtotime('2017-04-07 09:00:13');
    //     $end = strtotime('2017-04-08 20:00:43');
    //     $breakTime = $this->workHours->calculateHours($start, $end);    //休息時間
    //     $worktTime = floor($end - $start);
    //     $tct = $worktTime - $breakTime;
    // }
}

<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\CustomColor;
use App\Repositories\DashboardRepository;
use App\Traits\BoardProfile as ValidateDashboard;

class DashboardController extends Controller
{
    use ValidateDashboard;
    
    protected $boardRepo = null;

    public function __construct(DashboardRepository $boardRepo)
    {
        $this->boardRepo = $boardRepo;
    }

    /**
     * 依據設備整合識別碼 變更變色條件
     */
    public function changeCondition(CustomColor $request, $profile)
    {
        if (!$this->checkProfile($profile)) {
            return response()->json(['status' => 1, 'message' => 'This profile not found'], 404);
        }

        if ($this->compareNumber(request()->all()) === 1) {
            return response()->json(['status' => 1, 'message' => 'First number must bigger than second number'], 422);
        }

        $data = $this->boardRepo->updateColorCondition($profile, request()->all());
        return response()->json(['status' => 0]);
    }

    /**
     * 回傳所有設備整合識別碼
     */
    public function profileIndex()
    {
        return response()->json(['status' => 0, 'lineInfo' => $this->boardRepo->getProfiles()]);
    }
}

<?php

namespace App\Http\Controllers\api;

use \Curl\Curl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GetProduceOrder;
use App\Repositories\ProduceRepository;

class ProduceOrderController extends Controller
{
    private $produceService;
    protected $produceRepo;

	public function __construct(GetProduceOrder $produceService, ProduceRepository $produceRepo)
	{
        $this->produceService = $produceService;    // 正航API (GetProduceOrder)
        $this->produceRepo = $produceRepo;          // 取得本地錄入資訊 (ProduceRepository)
    }
    
    /**
     * 檢查 欲錄入之生產製令
     */
    public function checkMappingMoId($mo_id)
    {
        $data = $this->produceRepo->getRecordTag($mo_id);
    	$params = json_encode([['TypeId' => 'MO10', 'BillNo' => request()->mo_id]]);
        $check = $this->produceService->getProduceOrder($params);
        if ($check === 1) {
            return response()->json(['status' => 1, 'message' => 'web service connect error']);
        }
        if ($check === false) {
            return response()->json(['status' => 1, 'message' => 'The mo id is wrong']);
        }
        
        $tht = $this->getTht($params);
        if (is_null($tht)) {
            return response()->json(['status' => 1, 'message' => 'do not get THT from WebService']);
        }

        $Material = $this->produceService->getBillNoName($check['MaterialId']);
        $item_name = $Material ? $Material->MaterialName : '(無法取得料名)';
        return response()->json([
            'status' => 0,
            'blue_qty' => $data['B'],
            'red_qty' => $data['R'],
            'green_qty' => $data['G'],
            'qty' => $check['qty'],
            'name' => $check['name'],
            'message' => 'success',
            'mo_id' => request()->mo_id,
            'so_id' => $check['so_id'],
            'item' => $check['MaterialId'] ,
            'item_name' => $item_name
        ]);
    }
    
    /**
     * 檢查該製令是否有THT
     */
    private function getTht($params)
    {
        $material = $this->produceService->ProduceInfo($params);
        $tht = $this->produceService->getTht($material['item']);
        return !is_null($tht) ? $tht : null;
    }
}

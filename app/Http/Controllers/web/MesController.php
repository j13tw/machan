<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MesRepository;

class MesController extends Controller
{
    protected $mesRepo = null;

    public function __construct(MesRepository $mesRepo)
    {
        $this->mesRepo = $mesRepo;
    }

    public function rfidPage()
    {
        return view('mes.rfidDataPage');
    }

    public function moRfidPage()
    {
        return view('mes.moRfidDataPage');
    }

    public function mappingPage()
    {
        return view('mes.inputModulePage');
    }

    public function getRfidData()
    {
        $param = request()->all();
        $data = $this->mesRepo->rfidData($param);
        return response()->json($data);
    }

    public function getRfidHistory()
    {
        $param = request()->only('mac_id');
        $data = $this->mesRepo->rfidHistory($param);
        return response()->json($data);
    }

    public function getMoRfid()
    {
        $param = request()->all();
        $data = $this->mesRepo->moRfidData($param);
        return response()->json($data);
    }

    public function getMoRfidHistory()
    {
        $param = request()->all();
        $data = $this->mesRepo->moRfidHistory($param);
        return response()->json($data);
    }
}

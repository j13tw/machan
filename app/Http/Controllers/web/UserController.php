<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Register;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $userRepo = null;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function store(Register $request)
    {
        $data = $this->userRepo->create(request()->all());
        return response()->json(['status' => '0', 'message' => 'success']);
    }

    /**
     * 修改使用者資訊
     */
    public function update()
    {
        $user = auth()->user();
        $data = $this->userRepo->update($user, request()->all());
        if ($data) {
            return redirect('/');
        }
    }

    /**
     * 回傳個人資訊修改頁面
     */
    public function edit()
    {
        return view('user/edit', ['user' => auth()->guard('web')->user()]);
    }
}

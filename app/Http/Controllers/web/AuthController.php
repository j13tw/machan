<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('logout');
    }

    public function auth()
    {
        $data = request()->only('account', 'password');
        if (auth()->guard('web')->attempt($data)) {
            return response()->json(['status' => 0, 'message' => 'success']);
        }
        return response()->json(['status' => 1, 'message' => 'invalid credential'], 401);
    }

    public function logout()
    {
        auth()->guard('web')->logout();
        return redirect('login');
    }
}

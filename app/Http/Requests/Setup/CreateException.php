<?php

namespace App\Http\Requests\Setup;

use Illuminate\Foundation\Http\FormRequest;

class CreateException extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ex_code' => 'required|unique:setup_exceptions,ex_code',
            'exception' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request
     */
    public function messages()
    {
        return [
            'ex_code.required' => 'ex code does not allow empty',
            'ex_code.unique' => '異常碼已重複',
            'exception.required' => 'exception does not allow empty', 
        ];
    }
}

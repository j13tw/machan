<?php

namespace App\Http\Requests\Setup;

use Illuminate\Foundation\Http\FormRequest;

class CreateSuspend extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sp_code' => 'required|unique:setup_suspends,sp_code',
            'suspend' => 'required'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     */
    public function messages()
    {
        return [
            'sp_code.required' => '請輸入除外碼',
            'sp_code.unique' => '除外碼已重複',
            'suspend.required' => '請輸入除外原因'
        ];
    }
}

<?php

namespace App\Http\Requests\rfid;

use Illuminate\Foundation\Http\FormRequest;

class BatReprint extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qc_pass' => 'required|exists:mes_qc_pass_mos,qc_pass',
        ];
    }
}

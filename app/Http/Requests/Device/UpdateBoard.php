<?php

namespace App\Http\Requests\Device;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBoard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dashboard_name' => 'required',
            'type' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the requests.
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'dashboard_name.required' => '請輸入電子看板名稱',
            'type.required' => '請選擇看板類型',
        ];
    }
}

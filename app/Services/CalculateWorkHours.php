<?php

namespace App\Services;

/**
* 	處理所有工作時間
*/
class CalculateWorkHours
{
	private $lunchBreak;
	private $dinnerBreak;
	private $breakOne;
	private $breakTwo;
	private $breakThree;
	private $breakFour;

	public function __construct()
	{
		//一般無加班
        $this->lunchBreak = strtotime(date('Y-m-d') . ' 12:00:00');		//60分
        $this->breakOne = strtotime(date('Y-m-d') . ' 10:00:00');     	//10分
        $this->breakTwo = strtotime(date('Y-m-d') . ' 15:00:00');     	//10分
        //一般加班
        $this->dinnerBreak = strtotime(date('Y-m-d') . ' 17:20:00');  	//30分
        $this->breakThree = strtotime(date('Y-m-d') . ' 19:20:00');    	//10分

        //夏令無加班 4,5,6,7,8,9月
        $this->SlunchBreak = strtotime(date('Y-m-d') . ' 12:00:00');	//60分
        $this->SbreakOne = strtotime(date('Y-m-d') . ' 10:00:00');		//10分
        $this->SbreakTwo = strtotime(date('Y-m-d') . ' 14:30:00');		//30分
        $this->SbreakThree = strtotime(date('Y-m-d') . ' 16:50:00');	//10分
        //夏令有加班 4,5,6,7,8,9月
        $this->SdinnerBreak = strtotime(date('Y-m-d') . ' 18:20:00');  	//30分
        $this->SbreakFour = strtotime(date('Y-m-d') . ' 22:10:00');    	//10分

        //冬令無加班 10,11,12,1,2,3月
        $this->WlunchBreak = strtotime(date('Y-m-d') . ' 12:00:00');	//60分
        $this->WbreakOne = strtotime(date('Y-m-d') . ' 10:00:00');		//10分
        $this->WbreakTwo = strtotime(date('Y-m-d') . ' 16:50:00');		//10分
        //冬令有加班 10,11,12,1,2,3月
        $this->WdinnerBreak = strtotime(date('Y-m-d') . ' 17:20:00');  	//30分
        $this->WbreakFour = strtotime(date('Y-m-d') . ' 21:10:00');    	//10分
	}
	public function checkMonth()
	{
		$nowMonth = date('m');
		if ($nowMonth == '04' || $nowMonth == '05' || $nowMonth == '06' || $nowMonth == '07' || $nowMonth == '08' || $nowMonth == '09') {
			return 'summer';
		}
		if ($nowMonth == '10' || $nowMonth == '11' || $nowMonth == '12' || $nowMonth == '01' || $nowMonth == '02' || $nowMonth == '03') {
			return 'winter';
		}
	}
	public function generalWorkHours($start, $end)
	{
		$work = 0;
		if ($start < $this->lunchBreak && $end > $this->lunchBreak) {
			$work += 3600;
		}
		if ($start < $this->breakOne && $end > $this->breakOne) {
			$work += 600;
		}
		if ($start < $this->breakTwo && $end > $this->breakTwo) {
			$work += 600;
		}
		if ($start < $this->dinnerBreak && $end > $this->dinnerBreak) {
			$work += 1800;
		}
		if ($start < $this->breakThree && $end > $this->breakFour) {
			$work += 600;
		}
		return $work;
	}
	public function summerWorkHours($start, $end)
	{
		$work = 0;
		if ($start < $this->SlunchBreak && $end > $this->SlunchBreak) {
			$work += 3600;
		}
		if ($start < $this->SbreakOne && $end > $this->SbreakOne) {
			$work += 600;
		}
		if ($start < $this->SbreakTwo && $end > $this->SbreakTwo) {
			$work += 1800;
		}
		if ($start < $this->SbreakThree && $end > $this->SbreakThree) {
			$work += 600;
		}
		if ($start < $this->SdinnerBreak && $end > $this->SdinnerBreak) {
			$work += 1800;
		}
		if ($start < $this->SbreakFour && $end > $this->SbreakFour) {
			$work += 600;
		}
		return $work;
	}
	public function winterWorkHours($start, $end)
	{
		$work = 0;
		if ($start < $this->WlunchBreak && $end > $this->WlunchBreak) {
			$work += 3600;
		}
		if ($start < $this->WbreakOne && $end > $this->WbreakOne) {
			$work += 600;
		}
		if ($start < $this->WbreakTwo && $end > $this->WbreakTwo) {
			$work += 600;
		}
		if ($start < $this->WdinnerBreak && $end > $this->WdinnerBreak) {
			$work += 1800;
		}
		if ($start < $this->WbreakFour && $end > $this->WbreakFour) {
			$work += 600;
		}
		return $work;
	}
	public function calculateHours($start, $end)
	{
		$month = $this->checkMonth();
		switch ($month) {
			case 'summer':
				$work = $this->summerWorkHours($start, $end);
				return $work;
				break;
			case 'winter':
				$work = $this->winterWorkHours($start, $end);
				return $work;
				break;
			default:
				$work = $this->generalWorkHours($start, $end);
				return $work;
				break;
		}
	}
}
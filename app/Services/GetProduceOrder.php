<?php

namespace App\Services;

use Curl\Curl;

/**
* 	處理所有與正航API溝通
*/

class GetProduceOrder
{
	private $curl;
	private $bodyStart;
	private $bodyEnd;
	private $strData;

	public function __construct(Curl $curl)
	{
		$this->curl = $curl;
		$this->bodyStart = '<?xml version="1.0" encoding="utf-8" ?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ExecuteProc xmlns="http://tempuri.org/"><groupId>'.env('GROUP_ID').'</groupId><language/>zh-TW<language/><userId>'.env('USER_ID').'</userId><password>'.env('PASSWORD').'</password><progId>ppProduceOrder</progId><methodName>GetProduceOrder</methodName><wParams><anyType xsi:type="xsd:string">';
		$this->bodyEnd = '</anyType></wParams><ucoInvoke>false</ucoInvoke></ExecuteProc></soap:Body></soap:Envelope>';
		$this->strData = ['</ExecuteProcResult></ExecuteProcResponse></soap:Body></soap:Envelope>', '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ExecuteProcResponse xmlns="http://tempuri.org/"><ExecuteProcResult xsi:type="xsd:string">'];
	}

	public function connectWS($params)
	{
		$this->curl->setHeader('Content-Type', 'text/xml');
		$test = $this->curl->post('http://'.env('WS_API_IP').'/WebService/CAPInteropServiceEx.asmx', $this->bodyStart.$params.$this->bodyEnd);
		$data = str_replace($this->strData[0], '', $this->curl->rawResponse);
		$data = str_replace($this->strData[1], '', $data);
		$data = json_decode($data);
		return $data;
	}

	// 生產製令單 檢查單號是否存在
	public function getProduceOrder($params)
	{
		$data = $this->connectWS($params);
		if (!$data) {	// 若data變數回傳null 代表無法連接Web Service
			return 1;
		}
    	$array = ($data[0]->TypeId) ? ['qty' => (int)$data[0]->ProduceQty, 'name' => $data[0]->BizPartnerName,'so_id' => $data[0]->FromBillNo, 'MaterialId' => $data[0]->MaterialId] : [];
		return is_null($data[0]->TypeId) ? false : $array;
	}

	// 檢查 生產製令 產品數量 是否一致
	public function checkProduceQty($params, $macSum)
	{
		$data = $this->connectWS($params);
		if (!$data) {
			return 1;
		}
		return ($data[0]->ProduceQty != $macSum - 2) ? 1 : 0;
	}

	// 取得 生產製令 單號 資訊
	public function produceInfo($params)
	{
		$data = $this->connectWS($params);
		if (!$data) {
			return 1;
		}
		return [
			'so_id' => !is_null($data) ? $data[0]->FromBillNo : '',
			'org_id' => !is_null($data) ? $data[0]->_P_A1_FactoryId : '',
			'customer_id' => !is_null($data) ? $data[0]->BizPartnerId : '',
			'customer_name' => !is_null($data) ? $data[0]->BizPartnerName : '',
			'mo_qty' => !is_null($data) ? $data[0]->ProduceQty : '',
			'date' => !is_null($data) ? $data[0]->BillDate : '',
			'item' => !is_null($data) ? $data[0]->MaterialId : '',
			'completion_date' => !is_null($data) ? $data[0]->DemandCompleteDate : '',
		];
	}

	public function getTht($item)
	{
		$url = 'http://'.env('WS_MA_IP').'/api/ANPSV';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->get($url, array('$filter' => "CU_MaterialId eq '".$item."' and CU_OrgId eq '".env('ORG_ID')."'"));
		$productData = json_decode($this->curl->rawResponse);
		if (!is_null($productData)) {
			if (!empty($productData[0]->CU_RWTHT) && $productData[0]->CU_RWTHT !== 0) {
				return [
					'tht' => $productData[0]->CU_RWTHT
				];
			}
		}
	}

	public function getBillNoName($item)
	{
		$url = 'http://'.env('WS_MA_IP').'/api/MaterialGroupV/'.$item;
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->get($url);
		$BillNoName = json_decode($this->curl->rawResponse);
		return $BillNoName;
	}
}

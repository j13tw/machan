<?php

namespace App\Repositories;

use DB;
use App\Entities\User;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesMoRfid;
use App\Entities\MesRecord;
use App\Entities\MesRegister;
use App\Entities\MesQcPassMo;
use App\Entities\SetupLine;
use App\Entities\Notification;
use App\Entities\AppUnfinish;
use App\Entities\AppSuspend;

class NotifyRepository
{
	// 依據平板MAC 列出該線別 所有通知
	public function listIndex(User $user)
	{
		$data = [];
		$lineInfo = SetupLine::where('company_id', $user->company_id)
				->where('org_id', $user->org_id)
				->where('routing', $user->routing)
				->where('line_id', $user->line_id)
				->first();		
		$lineName = $lineInfo->line_name;
		$data = array_merge($data,
			$this->getPeople($lineName),
			$this->getLastOne($lineName),
			$this->getChangeTime($lineName),
			$this->getReprint($lineName),
			$this->getNonGreen($lineName),
			$this->getNonStart($lineName),
			$this->getUnfinished($lineName),
			$this->getStartTag($lineName),
			$this->getEndTag($lineName),
			$this->getSuspendChange($lineName),
			$this->getSuspendWork($lineName),
			$this->getForcePrint($lineName)
		);		
		return count($data) !== 0 ? $this->sortArray($data) : null;
	}

	// 依建立時間 降冪排序
	private function sortArray($data)
	{
		foreach ($data as $key => $row) {
			$arrayTime[$key] = $row['updated_at'];
		}
		array_multisort($arrayTime, SORT_DESC, $data);
		return $data;
	}

	// 開線人數通知
	public function getPeople($lineName)
	{
		$datas = $this->getNotification($lineName, 'N001', 1);
		return 
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N001',
						'mo_id' => $data->relatedMo->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'start_time' => $data->relatedMo->start_time,
						'customer_name' => $data->relatedMo->customer_name,
						'standard_human' => 18,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 換模換線通知
	public function getChangeTime($lineName)
	{
		$datas = $this->getNotification($lineName, 'N002');
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N002',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_fin_date' => !is_null($data->relatedMo->finish_date) ? $data->relatedMo->finish_date : '',
						'mo_fin_time' => !is_null($data->relatedMo->finish_time) ? $data->relatedMo->finish_time : '',
						'change_time' => $data->relatedMo->change_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 開始時間異常通知_無Green Tag開始
	public function getNonGreen($lineName)
	{
		$datas = $this->getNotification($lineName, 'N003', 1);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N003',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'first_start_time' => $data->relatedMo->first_start_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 寫入開始區TAG不合理讀取通知 N004
	public function getStartTag($lineName)
	{
		$datas = $this->getNotification($lineName, 'N004');
		return
			$datas->map(function ($item) {
				return [
					'id' => $item->id,
					'type' => 'N004',
					'mac_id' => $item->mac_id,
					'updated_at' => $item->updated_at
				];
			})->toArray();
	}

	// 寫入完工區TAG不合理讀取通知 N008
	public function getEndTag($lineName)
	{
		$datas = $this->getNotification($lineName, 'N008');
		return
			$datas->map(function ($item) {
				return [
					'id' => $item->id,
					'type' => 'N008',
					'mac_id' => $item->mac_id,
					'updated_at' => $item->updated_at
				];
			})->toArray();
	}

	// 開始時間異常通知_完工無開始
	public function getNonStart($lineName)
	{
		$datas = $this->getNotification($lineName, 'N009', 1);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$finTime = $this->getProFinTime($data->mo_id, $data->mac_id);	// 取得產品完工時間
					return [
						'id' => $data->id,
						'type' => 'N009',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'human' => $data->relatedMo->acture_human === 0 ? 18 : $data->relatedMo->acture_human,
						'rfid_type' => $data->rfid_type,
						'mac_id' => $data->mac_id,
						'THT' => $data->relatedMo->tht,
						'finish_time' => $finTime,
						'es_start_time' => date('H:i:s', strtotime($finTime) - ($data->relatedMo->tht * $data->relatedMo->qty / 18)),
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 成品完成時間
	private function getProFinTime($moid, $macid)
	{
		$data = MesRfid::where('mo_id', $moid)
					->where('mac_id', $macid)
					->first();
		return $data ? date('H:i:s', strtotime($data->end_datetime)) : '';
	}

	// 尾數異常通知
	public function getUnfinished($lineName)
	{
		$datas = $this->getNotification($lineName, 'N011', 1);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N011',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'qty' => $data->relatedMo->qty,
						'accumulator_start' => $data->relatedMo->accumulator_start,
						'accumulator_fin' => $data->relatedMo->accumulator_fin,
						'mo_fin_date' => $data->relatedMo->finish_date,
						'mo_fin_time' => $data->relatedMo->finish_time,
						'unfinishes' => $this->getNgTag(new AppUnfinish, $data->mo_id),	// tag array
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// QC PASS強制列印通知
	public function getForcePrint($lineName)
	{
		$datas = $this->getNotification($lineName, 'N016');
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$recordQty = MesRecord::where('mo_id', $data->mo_id)
									->where('rfid_type', 'B')
									->count();
					$forceInfo = $this->getForceQcPass($data->mo_id, $data->mac_id);
					return [
						'id' => $data->id,
						'type' => 'N016',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'qty' => $data->relatedMo->qty,
						'recordQty' => $recordQty,
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_start_qty' => $data->relatedMo->accumulator_start,	// 線別開工數
						'mo_fin_qty' => $data->relatedMo->accumulator_fin,	// 線別完工數 暫時取mes_mos 因為無法得知線別
						'print_count' => $this->getForceCount($data->mo_id),
						'qc_pass' => $forceInfo ? $forceInfo->qc_pass : '',
						'print_date' => $data->infor_date,
						'print_time' => $data->infor_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 取得製令強制列印次數
	private function getForceCount($mo_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('force_print_time', '<>', '')
			->count();
	}

	// 取得製令強制列印之QC PASS
	private function getForceQcPass($mo_id, $mac_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('mac_id', $mac_id)
			->first();
	}
	
	// QC PASS重複列印通知
	public function getReprint($lineName)
	{
		$datas = $this->getNotification($lineName, 'N017');
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$recordQty = MesRecord::where('mo_id', $data->mo_id)
									->where('rfid_type', 'B')
									->count();
					$reprintData = $this->getReprintInfo($data);
					$qcpass = $reprintData === 1 ? [] : $reprintData; 
					$batReprintInfo = $this->getBatchReprint(new NtfQcPass, $data->id);
					return [
						'id' => $data->id,
						'type' => 'N017',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'rfid_status' => $data->rfid_status,
						'mac_id' => $data->mac_id,
						'qty' => $data->relatedMo->qty,
						'recordQty' => $recordQty,
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_fin_date' => $data->relatedMo->finish_date,
						'mo_fin_time' => $data->relatedMo->finish_time,
						'qc_pass' => count($qcpass) === 0 ? '' : $qcpass['qc_pass'],	// 批次列印 則不顯示qc_pass 及 一般列印時間
						'print_date' => count($qcpass) === 0 ? '' : $qcpass['print_date'],
						'print_time' => count($qcpass) === 0 ? '' : $qcpass['print_time'],	// 批次列印 reprint_date/time 直接顯示通知時間
						'reprint_date' => count($qcpass) === 0 ? $batReprintInfo->infor_date : $qcpass['reprint_date'],
						'reprint_time' => count($qcpass) === 0 ? $batReprintInfo->infor_time : $qcpass['reprint_time'],
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 列出未列印QC PASS之TAG
	private function listNonPrint($moid)
	{
		$data = [];
		$nonPrintDatas = MesQcPassMo::where('mo_id', $moid)
								->where('rfid_type', 'B')
								->where('qc_pass', '')
								->get();
		foreach ($nonPrintDatas as $key => $nonPrintData) {
			array_push($data, $nonPrintData->mac_id);
		}
		return $data;
	}

	// 最後一筆通知
	public function getLastOne($lineName)
	{
		$datas = $this->getNotification($lineName, 'N007');
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$lastPro = $this->findLastProduct($data);
					return [
						'id' => $data->id,
						'type' => 'N007',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'qty' => $data->relatedMo->qty,
						'accumulator_fin' => $data->relatedMo->accumulator_fin,
						'pro_start_date' => $lastPro ? date('Y-m-d', strtotime($lastPro->start_datetime)) : '',
						'pro_start_time' => $lastPro ? date('H:i:s', strtotime($lastPro->start_datetime)) : '',
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 除外工時 - 換模換線通知
	public function getSuspendChange($lineName)
	{
		$datas = $this->getNotification($lineName, 'N018', 1);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N018',
						'infor_date' => $data->infor_date,
						'infor_time' => $data->infor_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 除外工時 - 作業效率異常通知
	public function getSuspendWork($lineName)
	{
		$datas = $this->getNotification($lineName, 'N019', 1);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N019',
						'infor_date' => $data->infor_date,
						'infor_time' => $data->infor_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 尋找重複列印資訊
	private function getReprintInfo($data)
	{
		$reprint = MesQcPassMo::where('mo_id', $data->mo_id)
						->where('mac_id', $data->mac_id)
						->where('reprint', 1)
						->first();
		if ($reprint) {
			return [
				'qc_pass' => $reprint->qc_pass,
				'print_date' => $reprint->print_date,
				'print_time' => $reprint->print_time,
				'reprint_date' => $reprint->reprint_date,
				'reprint_time' => $reprint->reprint_time
			];
		}
		return 1;
	}

	// 回傳重複列印 (批次列印) 通知資訊
	private function getBatchReprint(NtfQcPass $entity, $id)
	{
		return $entity->find($id);
	}

	// 利用模型找出該線別 未回覆通知
	private function getNotification($lineName, $code, $notiType = null)
	{
		// notiType 預設值為null
		$days = $notiType === 1 ? '-6 days' : '-2 days';
		$diffDate = date('Y-m-d', strtotime($days));
		return Notification::with('relatedMo')
			->where('code', $code)
			->where('reply', 0)
			->where('line_name', $lineName)
			->whereBetween(DB::raw('date(created_at)'), [$diffDate, date('Y-m-d')])
			->get();
	}

	// 尋找最後一筆生產之產品
	public function findLastProduct($data)
	{
		return MesRfid::where('mo_id', $data->mo_id)
			->orderBy('start_datetime', 'DESC')
			->first();
	}

	// 輸入開線人數 N001
	public function postPeople($id, $humanQty)
	{
		$notify = Notification::where('id', $id)
					->where('code', 'N001')
					->first();
		if (!$notify) {
			return 1;
		}

		if (! $moInfo = $notify->relatedMo) {
			return 2;
		}
		$notify->update([
			'reply' => 1,
			'reply_date' => date('Y-m-d'),
			'reply_time' => date('H:i:s')
		]);
		$moInfo->update([
			'acture_human' => $humanQty,
			'f_completion_time' => date('H:i:s', strtotime(date('Y-m-d H:i:s')) + ($moInfo->tht * $moInfo->qty / $humanQty))
		]);
	}

	// 輸入製令開始時間 N003
	public function postMoTime($id, $startTime)
	{
		$notify = Notification::where('id', $id)
					->where('code', 'N003')
					->first();
		if (!$notify) {
			return 1;
		}

		if (! $moInfo = $notify->relatedMo) {
			return 2;
		}
		
		$notify->update([
			'reply' => 1,
			'reply_date' => date('Y-m-d'),
			'reply_time' => date('H:i:s')
		]);
		$moInfo->update([
			'start_time' => $startTime,
			'first_start_time' => $startTime
		]);
	}

	// 輸入產品開始時間 N009
	public function postProStart($id, $startTime)
	{
		$rfidEntites = [new MesRfid, new MesMoRfid];
		$notify = Notification::where('id', $id)
					->where('code', 'N009')
					->first();
		if (!$notify) {
			return 1;
		}

		if (! $moInfo = $notify->relatedMo()) {
			return 2;
		}

		foreach ($rfidEntites as $key => $entity) {
			$entity->where('mo_id', $moInfo->mo_id)
				->where('mac_id', $notify->mac_id)
				->update([
					'start_date' => date('Y-m-d'),
					'start_time' => $startTime
				]);
		}

		$notify->update([
			'reply' => 1,
			'reply_date' => date('Y-m-d'),
			'reply_time' => date('H:i:s')
		]);
	}

	// 輸入尾數異常原因 N011
	public function postUnfinished($id, array $params)
	{
		$notify = Notification::where('id', $id)
					->where('code', 'N011')
					->first();
		if (!$notify) {
			return 1;
		}

		if (! $rfids = $notify->relatedApp()) {
			return 2;
		}
		$rfids->where('mo_id', $notify->mo_id)
			->whereIn('mac_id', $params['mac_id'])
			->update([
				'reply' => 1,
				'reply_time' => date('H:i:s'),
				'reply_date' => date('Y-m-d'),
				'except_reason' => json_encode($params['reason'])
			]);
		$nonReply = $this->getNgTag(new AppUnfinish, $notify->mo_id);
		if (count($nonReply) === 0) {
			$notify->update([
				'reply' => 1,
				'reply_date' => date('Y-m-d'),
				'reply_time' => date('H:i:s')
			]);
		}
	}

	// 回傳該製令 未回覆尾數
	private function getNgTag(AppUnfinish $entity, $moid)
	{
		return $entity->where('mo_id', $moid)
			->where('reply', 0)
			->select('mac_id')
			->get()
			->map(function ($item) {
				return $item->mac_id;
			})->toArray();
	}

	// 輸入除外工時
	public function postSuspend($id, array $params)
	{
		$notify = Notification::where('id', $id)
					->where(function ($query) {
						$query->where('code', 'N018')
							->orWhere('code', 'N019');
					})->first();
		if (!$notify) {
			return 1;
		}
		
		if (!$notify->relatedSuspend) {
			return 2;
		}
		$notify->relatedSuspend()->update($params);
		// 補上更新回覆狀態
	}

	// 回覆通知 (更新為已讀)
	public function replyNotification($id)
	{
		$notify = Notification::find($id);
		$notify->reply = 1;
		$notify->reply_date = date('Y-m-d');
		$notify->reply_time = date('H:i:s');
		$notify->save();
	}
}

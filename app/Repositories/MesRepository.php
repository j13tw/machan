<?php
namespace App\Repositories;

use DB;
use App\Entities\MesRfid;

class MesRepository
{
    // RFID數據
    public function rfidData(array $data)
    {
        if (empty($data['mac_id']) && empty($data['rfid_type']) && empty($data['rfid_status']) && empty($data['profile'])) {
            return DB::table('mes_rfids')
                    ->get();
        }
        return 
            DB::table('mes_mos')
                ->join('mes_rfids', 'mes_mos.mo_id', '=', 'mes_rfids.mo_id')
                ->where('mes_rfids.mac_id', 'LIKE', $data['mac_id'])
                ->where('mes_rfids.rfid_type', $data['rfid_type'])
                ->where('mes_rfids.rfid_status', $data['rfid_status'])
                ->where('mes_mos.profile', $data['profile'])
                ->get();
    }

    // RFID報工紀錄
    public function rfidHistory(array $data)
    {
        // 待修
        return DB::table('mes_rfids')
            ->where('mac_id', $data['mac_id'])
            ->select('id', 'start_datetime', 'so_id', 'mo_id', 'customer_id', 'customer_name', 'item', 'record_id', 'start_profile')
            ->get();       
    }

    // 製令RFID報工數據
    public function moRfidData(array $data)
    {
        if (empty($data['mo_id']) && empty($data['so_id']) && empty($data['date']) && empty($data['fin_date']) && empty($data['mo_status']) && empty($data['profile'])) {
            return DB::table('mes_mos')
                    ->get();
        }
        return
            DB::table('mes_mos')
                ->where('mo_id', $data['mo_id'])
                ->where('so_id', $data['so_id'])
                ->where('date', $data['date'])
                ->where('finish_date', $data['fin_date'])
                ->where('mo_status', $data['mo_status'])
                ->where('profile', $data['profile'])
                ->get();
    }

    // 製令RFID紀錄
    public function moRfidHistory(array $data)
    {
        
    }
}

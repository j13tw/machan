<?php

namespace App\Repositories;

use DB;
use App\Entities\User;
use App\Entities\EbMo;
use App\Entities\EbWeb;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesOffWork;
use App\Entities\MesTimeflag;
use App\Entities\SetupOrg;
use App\Entities\SetupTablet;
use App\Entities\SetupLine;
use App\Entities\SetupReader;

class DeviceRepository
{
	/**
	 * 上班按鈕 新增當日換模換線開始
	 */
	public function createChangeTime(User $user)
	{
		$lineInfo = $this->getLineInfo($user);
		return MesTimeflag::create([
			'profile' => $lineInfo->profile,
			'type' => 0,
			'date' => date('Y-m-d'),
			'time' => date('H:i:s'),
			'note' => '系統開機自動給值'
		]);
	}

	/**
	 * 下班按鈕
	 */
	public function postOffWork(User $user)
	{
		$lineInfo = $this->getLineInfo($user);
		$writeEb = $this->dataWriteBack($lineInfo->profile);
		$writeOff = $this->unfinishWriteBack();
		if (!is_null($writeEb) && $writeOff === true) {
			return true;
		}
		return false;
	}
	
	/**
	 * 取得登入者線別資訊
	 */
	private function getLineInfo(User $user)
	{
		return SetupLine::where('company_id', $user->company_id)
			->where('org_id', $user->org_id)
			->where('line_id', $user->line_id)
			->first();
	}

	/**
	 * 下班回寫當日績效
	 */
	public function dataWriteBack($profile)
	{
		$data = $this->getMoData($profile);
		if (empty($data)) {
			return [];
		}
		return EbWeb::create([
			'profile' => $data['profile'],
			'line_mo_qty' => $data['lineMoQty'],
			'day_mo_qty' => $data['moQty'],
			'day_accumulator_start' => $data['accuStart'],
			'day_accumulator_fin' => $data['accuFin'],
			'day_ng' => $data['accuNg'],
			'day_qty' => $data['accuMo'],
			'day_first_pass_rate' => $data['accuPass'],
			'day_change_lead_time' => $data['accuChange'],
			'object_equivalents' => $data['targetQty'],
			'accu_fin_equivalents' => $data['realQty'],
			'capacity_rate' => $data['capRate'],
		]);
	}

	/**
	 * 記錄當日未完工 TAG
	 */
	public function unfinishWriteBack()
	{
		$nonRfids = MesRfid::with('relatedMo')
						->where('rfid_status', '<>', 4)
						->where('rfid_type', 'B')
						->get();
		$nonRfids->map(function ($nonRfid) {
			MesOffWork::create([
				'profile' => $nonRfid->start_profile,
				'line_id' => $nonRfid->start_line,
				'off_work_date' => date('Y-m-d'),
				'off_work_time' => date('H:i:s'),
				'mac_id' => $nonRfid->mac_id,
				'rfid_type' => $nonRfid->rfid_type,
				'rfid_status' => $nonRfid->rfid_status,
				'mo_id' => $nonRfid->mo_id,
				'item' => $nonRfid->item,
				'mo_qty' => $nonRfid->relatedMo->qty,
				'mo_status' => $nonRfid->relatedMo->mo_status,
				'mo_start_date' => $nonRfid->relatedMo->date,
				'mo_start_time' => $nonRfid->relatedMo->start_time,
				'tht' => $nonRfid->relatedMo->tht,
				'finish' => $nonRfid->relatedMo->finish,
				'ng' => $nonRfid->relatedMo->nf_qty,
				'human' => 18,
				'acture_human' => $nonRfid->relatedMo->acture_human,
				'start_line' => $nonRfid->start_line,
				'start_profile' => $nonRfid->start_profile,
				'start_antenna_id' => $nonRfid->start_antenna_id,
				'start_date' => $nonRfid->start_datetime,
				'start_time' => $nonRfid->start_datetime,
				'first_one_time' => $nonRfid->relatedMo->first_start_time
			]);
		});
		return true;
	}

	/**
	 * 獲取該線別當日績效
	 */
	public function getMoData($profile)
	{
		$moData = DB::table('mes_mos')
				->join('mes_rfids', 'mes_mos.mo_id', '=', 'mes_rfids.mo_id')
				->where('mes_mos.profile', $profile)
				->selectRaw('mes_mos.change_time, mes_mos.tht, mes_mos.finish, accumulator_start, accumulator_fin, nf_qty, date(mes_rfids.start_datetime) as start_date, date(mes_rfids.end_datetime) as end_date')
				->distinct()
				->get()
				->filter(function ($item) {
					return $item->start_date === date('Y-m-d') || $item->end_date === date('Y-m-d');
				});
		if (count($moData) === 0) {
			return [];
		}
		$lineMoQty = $moData->where('finish', '<>', 1)->count();
		$moQty = $moData->where('finish', 1)->count();
		$accuStart = $moData->sum('accumulator_start');
		$accuFin = $moData->sum('accumulator_fin');
		$accuNg = $moData->sum('nf_qty');
		$accuMo = $moData->count();
		$accuPass = $accuStart !== 0 ? round($accuFin / $accuStart, 3) * 100 . '%' : '-';
		$accuChange = $this->calcChangeTime($moData);
		$targetQty = $this->calcTargetQty($moData, $profile);
		$realQty = $this->calcRealQty($moData);
		$rate = $targetQty !== 0 ? $realQty / $targetQty : '-';
		$capRate = $rate !== '-' ? round($rate, 3) * 100 . '%' : '-';

		return [
			'profile' => $profile,
			'lineMoQty' => $lineMoQty,
			'moQty' => $moQty,
			'accuStart' => $accuStart,
			'accuFin' => $accuFin,
			'accuNg' => $accuNg,
			'accuMo' => $accuMo,
			'accuPass' => $accuPass,
			'accuChange' => $accuChange,
			'targetQty' => $targetQty,
			'realQty' => $realQty,
			'capRate' => $capRate,
		];
	}

	/**
	 * 計算換模換線
	 */
	public function calcChangeTime($datas)
	{
		$changeTime = 0;
		foreach ($datas as $key => $data) {
			$changeTime = $changeTime + strtotime($data->change_time);
		}
		$result = empty($changeTime) ? '-' : gmdate('H:i:s', $changeTime);
		return $result;
	}

	/**
	 * 計算實際約當量
	 */
	public function calcRealQty($datas)
	{
		$realQty = 0;
		$accuFin = 0;
		foreach ($datas as $key => $data) {
			$accuFin = $accuFin + ($data->accumulator_fin - $data->nf_qty);
			$realQty = $realQty + floor($data->tht / 1347.2 * $accuFin);
		}
		return $realQty;
	}

	/**
	 * 計算目標約當量
	 */
	public function calcTargetQty($datas, $profile)
	{
		$relaxTimes = SetupOrg::get();
		$actureHuman = 0;
		$accuTargetQty = 0;
		foreach ($relaxTimes as $key => $relaxTime) {
			$queryTime = date('H:i') - $relaxTime->time;
			if ($queryTime === 0 || $queryTime === 1) {
				if (!is_null($queryTime)) {
					$accuTime = $relaxTimes->where('time', $relaxTime->time)->first();
					$times = strtotime(date('H:i:s')) - strtotime(date('08:00:00'));
					$data = $datas->where('profile', $profile)->first();
					if (is_null($data)) {
						$actureHuman = 18;
					} else {
						if ($data->acture_human === 0) {
							$actureHuman = 18;
						} else {
							$actureHuman = $data->acture_human;
						}
					}
					$accuTargetQty = ($times - $accuTime->accumulator_time * 60) / 1347.2 * $actureHuman;
					break;
				}
			}
		}
		return $accuTargetQty;
	}
}

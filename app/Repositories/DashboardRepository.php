<?php

namespace App\Repositories;

use DB;
use Carbon\Carbon;
use App\Entities\EbMo;
use App\Entities\EbWeb;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesQcPassMo;
use App\Entities\MesTimeflag;
use App\Entities\SetupOrg;
use App\Entities\SetupLine;

class DashboardRepository
{
	// 電子看板資料
	public function boardIndex($profile)
	{
		$result = [];
		$moInfo = [];
		$sqlRaw = [
			'TIME_TO_SEC(TIMEDIFF(now(), updated_at)) AS diff_time, '.
			'mo_id, qty, f_completion_time, start_time, finish_time, accumulator_start, accumulator_fin, '.
			'nf_qty, customer_name, item, acture_human'
		];

		// 取得通過Reader最新RFID
		$rfid = MesRfid::where('rfid_type', 'B')
					->where(function ($query) {
						$query->where('rfid_status', 3)
							->orWhere('rfid_status', 4);
					})->orderBy('updated_at', 'DESC')
					->first();	
		// 取得報工中製令 (已上線且持續動工)
		$moData = MesMo::where('profile', $profile)
					->selectRaw($sqlRaw[0])
					->orderBy('updated_at', 'DESC')
					->take(3)
					->get();
		$timeflag = $this->checkWorkOn($profile);	// 檢查上班
		$ebwebs = $this->checkWorkOff($profile);	// 檢查下班
		$offDate = !is_null($ebwebs) ? $ebwebs->date : '';	// 判斷績效結算是否存在 (下班) 回傳日期
		$workStatus = count($timeflag) !== 0 ? 0 : 1;
		$offStatus = $offDate === date('Y-m-d') ? 0 : 1;	// 判斷有無今日績效 有則表示已下班

		if (!is_null($rfid)) {
			$passStatus = $rfid->rfid_status === 4 ? 'end' : 'start';
			$passMoid = $rfid->mo_id;
			$passColor = $rfid->rfid_type;
		} else {
			$passStatus = '';
			$passMoid = '';
			$passColor = '';
		}

		$rank = 1;
		foreach ($moData as $key => $data) {
			if ($data->diff_time < 60) {			
				$moInfo[] = [
					'rank' => $rank++,
					'mo_id' => $data->mo_id,
					'qty' => $data->qty,
					'completion_date' => $data->f_completion_time,
					'start_time' => $data->start_time,
					'finish_time' => $data->finish_time,
					'accumulator_start' => $data->accumulator_start,
					// 'accumulator_fin' => $this->getFinish($data->mo_id),
					'accumulator_fin' => $data->accumulator_fin,
					'nf_qty' => $data->nf_qty,
					'customer_name' => $data->customer_name,
					'item' => $data->item,
					'actureHuman' => $data->acture_human,
					'different_time' => !is_null($data->finish_time) ? gmdate('H:i:s', strtotime($data->finish_time) - strtotime($data->f_completion_time)) : '-',
				];
			}
		}
		
		array_push($result, $moInfo, $passMoid, $passColor, $passStatus, $workStatus, $offStatus);
		return $result;
	}

	// 查詢本日是否上班 (有無當日換模換線)
	private function checkWorkOn($profile)
	{
		return MesTimeflag::where('profile', $profile)	// 判斷本日是否上班
			->where('date', date('Y-m-d'))
			->where('type', 0)
			->get();
	}

	// 查詢本日是否下班 (績效是否回寫)
	private function checkWorkOff($profile)
	{
		return DB::table('eb_webs')	// 判斷本日是否下班
			->selectRaw('id, profile, date(created_at) AS date')	
			->where('profile', $profile)
			->first();
	}

	// 績效看板
	public function accumulateData($param)
	{
		// index 0:累計投入 1:累計完工 2:累計尾數 3:已完成製令數 4:常駐製令數 5:累計換線 6:累計一次通過率 7:目標約當
		$accuInfo = []; 
		$columns = [['start_profile', 'start_datetime'], ['end_profile', 'end_datetime']];
		$rfids = new MesRfid;
		$lineInfo = SetupLine::where('profile', $param['profile'])
						->first();
		$moDatas = MesMo::where('profile', $param['profile'])
					->get();
		$data = DB::table('mes_mos')
				->where('profile', $param['profile'])
				->whereDate('finish_date', $param['date'])
				->selectRaw('SUM(nf_qty) as ng, COUNT(*) as fin_qty')
				->first();
		
		$accuInfo[0] = MesRfid::where('rfid_type', 'B')
						->where('start_profile', $param['profile'])
						->whereDate(DB::raw('date(start_datetime)'), $param['date'])
						->count();
		$accuInfo[1] = MesRfid::where('rfid_type', 'B')
						->where('end_profile', $param['profile'])
						->whereDate(DB::raw('date(end_datetime)'), $param['date'])
						->count();
		$accuInfo[2] = $data->ng;
		$accuInfo[3] = $data->fin_qty;
		$accuInfo[5] = '00:00:00';
		$accuInfo[6] = 0;
		if (!is_null($moDatas)) {
			$accuInfo[4] = $moDatas->where('finish', '<>', 1)->count();
			$prodQty = $moDatas->where('finish_date', $param['date'])->sum('prod_qty');
			$finQty = $moDatas->where('finish_date', $param['date'])->sum('fin_qty');
			foreach ($moDatas->where('date', $param['date']) as $key => $moData) {
				$tmp = '1970-01-01 00:00:00';
				$accuInfo[5] += strtotime($tmp) + strtotime('1970-01-01 '.$moData->change_time.'-8 hours');
			}
			if ($prodQty !== 0) {
				$accuInfo[6] = round($finQty / $prodQty, 2) * 100;
			}
		}

		$relaxTimes = SetupOrg::get();	// 計算休息時間
		foreach ($relaxTimes as $key => $relaxTime) {
			$queryTime = date('H:i') - $relaxTime->time;
			if ($queryTime === 0 || $queryTime === 1) {
				if (!is_null($queryTime)) {
					$accuTime = $relaxTimes->where('time', $relaxTime->time)->first();
					$times = strtotime(date('H:i:s')) - strtotime(date('08:00:00'));
					$data = $moDatas->where('date', $param['date'])
								->first();
					if (!$data) {
						$actureHuman = 18;
					} else {
						if ($data->acture_human === 0) {
							$actureHuman = 18;
						} else {
							$actureHuman = $data->acture_human;
						}
					}
					$accuTargetQty = ($times - $accuTime->accumulator_time * 60) / 1347.2 * $actureHuman;
					break;
				}
			}
		}

		$accuStart = 0;
		$accuEnd = 0;
		$accuRealQty = 0;
		foreach ($moDatas->where('date', $param['date']) as $key => $data) {
			$accuStart = $accuStart + $data->accumulator_start;
			$accuEnd = $accuEnd + $data->accumulator_fin;
			$accuRealQty = $accuRealQty + floor($data->tht / 1347.2 * $accuEnd);    // 累計實際約當量
		}

		return [
			'first_condition' => $lineInfo->first_condition,
			'second_condition' => $lineInfo->second_condition,
			'accuStart' => $accuInfo[0] ? $accuInfo[0] : 0,
			'accuEnd' => $accuInfo[1] ? $accuInfo[1] : 0,
			'accuNf' => $accuInfo[2] ? $accuInfo[2] : 0,
			'accuPassRate' => $accuInfo[6] . '%',
			'finCount' => $accuInfo[3],
			'accuRealQty' => $accuRealQty,
			'accuChangeTime' => $this->handleTimeError($accuInfo[5]),
			'moCount' => $accuInfo[4],
			'accuTargetQty' => floor($accuTargetQty),
			'qtyRate' => $accuTargetQty !== 0 ? round($accuRealQty / $accuTargetQty, 3) * 100 : '-'
		];		
	}

	private function handleTimeError($time)
	{
		try {
			return gmdate('H:i:s', $time) === '00:00:00' ? '-' : gmdate('H:i:s', $time);
		} catch (\Exception $e) {
			return '-';
		}
	}

	public function updateColorCondition($profile, $params)
	{
		return SetupLine::where('profile', $profile)
			->update($params);
	}

	public function getProfiles()
	{
		return 
			SetupLine::get()->map(function ($item) {
				return [
					'line_name' => $item->line_name,
					'profile' => $item->profile
				];
			});
	}
}

<?php

namespace App\Repositories;

use DB;
use App\Entities\User;
use App\Entities\EbMo;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesQcPass;
use App\Entities\MesRecord;
use App\Entities\MesRegister;
use App\Entities\MesQcPassMo;
use App\Entities\SetupEquip;
use App\Entities\SetupTablet;
use App\Entities\SetupLine;
use App\Entities\SetupReader;
use App\Entities\SetupAntenna;

class RfidRepository
{
	/**
	 * 錄入
	 */
	public function mappingRfid($params)
	{
		$types = ['G', 'R'];
		$tagInfo = [];
		foreach ($types as $key => $type) {
			$rfidInfo = MesRegister::whereIn('mac_id', $params['mac_id'])
					->where('rfid_type', $type)
					->first();
			$tagInfo[$type] = $rfidInfo ? $rfidInfo->mac_id : '';
		}

	    $checkMo = MesRegister::whereIn('mac_id', $params['mac_id'])
					->where('mo_id', $params['mo_id'])
					->first();
	    if ($checkMo) {
	    	return 1;
		}
		return [
			'start' => $tagInfo['G'],
			'end' => $tagInfo['R'],
			'mo_id' => $params['mo_id'],
			'complete_date' => $params['complete_date'],
			'mac_data' => $params['mac_id']
		];
	}

	/**
	 * 檢查 RFID 狀態
	 */
	public function checkRfidStatus(array $mac_id)
	{
		$data = [];
		$checkRfids = MesRegister::whereIn('mac_id', $mac_id)
					->where('rfid_status', '<>', 2)
					->get();
		foreach ($checkRfids as $key => $checkRfid) {
			$data[] = $checkRfid->mac_id;
		}
		return $data;	
	}

	/**
	 * 取得製令 RFID TAG 數量
	 */
	public function getTagQty($mo_id)
	{
		$data = [];
		$tags = MesRecord::groupBy('record_date')
					->where('mo_id', $mo_id)
					->get();
		foreach ($tags as $key => $tag) {
			$query = MesRecord::where('record_date', $tag->record_date)
						->where('mo_id', $mo_id)
						->where('rfid_type', 'B')
						->count();
			$data[] = [
				'count' => $query,
				'record_date' => $tag->record_date
			];
		}
		return $data;
	}

	/**
	 * 通過 Start 寫入資料
	 */
	public function rfidStart($params)
	{
		$device = SetupAntenna::with('relatedLine')
					->where('device_id', $params['reader_id'])
					->where('port_no', $params['antenna_id'])
					->first();
		$startData = $this->startModule($params['mac_id'], $device);
		if ($startData === 1) {
			return 1;
		}

		$startData['reader_id'] = $params['reader_id'];
		$startData['antenna_id'] = $device->id;
		$startData['antenna_type'] = $device->antenna_type;
		$startData['port_id'] = $params['antenna_id'];
		return $startData;
	}

	/**
	 * 完工無開始 (補償開始區資訊)
	 */
	public function checkRfidStart(User $user, $params)
	{
		// $device = SetupTablet::with('relatedAntenna.relatedLine')
		// 			->where('mac_id', $params['tablet_id'])
		// 			->first();
		$lineInfo = $this->getProfile($user);
		$startData = $this->startModule($params['mac_id'], $lineInfo);
		if ($startData === 1) {
			return 1;
		}
		return $startData;
	}

	/**
	 * 開始區模組
	 */
	private function startModule($mac_id, $lineInfo)
	{
		$macData = MesRegister::with('relatedMo')	// 檢查該TAG狀態
					->where('mac_id', $mac_id)
					->where('rfid_status', 1)
					->first();
		if (!$macData) {
			return 1;
		}
		return [
			'line_id' => $lineInfo->line_id,
			'line_name' => $lineInfo->line_name,
			'routing' => $lineInfo->routing,
			'profile' => $lineInfo->profile,
			'mo_id' => $macData->mo_id,
			'mac_id' => $macData->mac_id,
			'item' => $macData->relatedMo->item,
			'start_date' => date('Y-m-d'),
			'start_time' => date('H:i:s'),
			'start_datetime' => date('Y-m-d H:i:s'),
			'rfid_type' => $macData->rfid_type,
			'customer_id' => $macData->relatedMo->customer_id,
			'customer_name' => $macData->relatedMo->customer_name,
			'reader_id' => '',
			'antenna_id' => '',
			'antenna_type' => '',
			'port_id' => ''
		];
	}

	/**
	 * 通過 End 寫入資料
	 */
	public function rfidEnd(User $user, $params)
	{
		$macData = MesRegister::with('relatedMo')
					->where('mac_id', $params['mac_id'])
					->first();
		switch ($macData->rfid_status) {
			case 0:
				return 1;
			case 2:
				return 2;
			case 4:
				return 3;
		}
		$qcPass = $this->checkQcRepeat($macData->mo_id);
		return [
			'status' => 0,	// 0:Red結案 1:強制結案
			'line_id' => $user->line_id,
			'profile' => $this->getProfile($user)->profile,
			'mo_id' => $macData->mo_id,
			'mac_id' => $macData->mac_id,
			'customer_id' => $macData->relatedMo->customer_id,
			'customer_name' => $macData->relatedMo->customer_name,
			'antenna_id' => $this->getAntenna($user)->id,
			'qc_pass' => $macData->rfid_type === 'B' ? $qcPass : '',
			'qc' => $user->name,
			'rfid_type' => $macData->rfid_type,
			'item' => $macData->relatedMo->item,
			'rfid_status' => $macData->rfid_status,
			'now' => date('Y-m-d H:i:s')
		];
	}

	/**
	 * 完工模組 依據使用者線別 取得antenna_id
	 */
	private function getAntenna(User $user)
	{
		return SetupAntenna::where('org_id', $user->org_id)
			->where('routing', $user->routing)
			->where('line_id', $user->line_id)
			->where('antenna_type', 2)	// 2:完工區 antenna 天線
			->first();
	}

	/**
	 * 依據使用者 回傳設備整合識別碼 (僅線綁線別人員)
	 */
	public function getProfile(User $user)
	{
		return SetupLine::where('company_id', $user->company_id)
			->where('line_id', $user->line_id)
			->where('routing', $user->routing)
			->where('org_id', $user->org_id)
			->first();
	}

	/**
	 * 檢查 QC PASS 是否重複
	 */
	public function checkQcRepeat($mo_id)
	{
		$checkFinQty = MesRfid::where('mo_id', $mo_id)
						->where('qc_pass', '<>', '')
						->where('rfid_type', 'B')
						->where('rfid_status', 4)
						->count();
		$checkNum = $checkFinQty + 1;
		$qcPass = '';
		for ($i = $checkNum; ; $i++) {
			$checkNumber = MesQcPassMo::where('mo_id', $mo_id)
							->where('qc_pass', $mo_id.'_'.$i)
							->first();
			if (!$checkNumber) {
				$qcPass = $mo_id.'_'.$i;
				break;
			}
		}
		return $qcPass;
	}

	/**
	 * 核對 QC PASS (一般列印 重複列印)
	 */
	public function printQcPass(User $user, array $params)
	{
		$qcpassMo = new MesQcPassMo;
		$check = $qcpassMo->where('mac_id', $params['mac_id'])
					->where('qc_pass', $params['qc_pass'])
					->first();
        $checkPass = $qcpassMo->where('mac_id', $params['mac_id'])
        				->orderBy('created_at', 'DESC')
						->first();
		if (!$check) {
			return 1;
		}
		$checkForce = $qcpassMo->where('mo_id', $check->mo_id)
						->where('printed', 0)
						->where('mac_id', $params['mac_id'])
						->where('rfid_type', 'B')
						->where('force_print_time', null)
						->first();
		if ($checkPass->printed == 1 && $params['state'] == 0 && !is_null($checkForce)) {
			return 2;
		}
		return [
			'mo_status' => $check->relatedMo->mo_status,
			'profile' => $check->relatedMo->profile,
			'rfid_type' => $check->rfid_type,
			'rfid_status' => $check->rfid_status,
			'item' => $this->getItem($check->mo_id),
			'qc_pass' => $params['qc_pass'],
			'qc' => $user->name,
			'mac_id' => $params['mac_id'],
			'mo_id' => $check->mo_id,
			'state' => $params['state'],
			'message' => $params['message']
		];
	}

	/**
	 * 批次重複列印
	 */
	public function reprint(array $data)
	{
		$tables = ['mes_qc_passes', 'mes_qc_pass_mos'];
		foreach ($tables as $key => $table) {
			DB::table($table)
			->whereIn('qc_pass', $data['qc_pass'])
			->update([
				'reprint_date' => date('Y-m-d'),
				'reprint_time' => date('H:i:s'),
				'reprint' => 1,
				'printed' => 1
			]);
		}
	}

	/**
	 * 取得物料代號
	 */
	private function getItem($moid)
	{
		$data = MesQcPassMo::where('mo_id', $moid)
					->first();
		return $data ? $data->item : '';
	}

	/**
	 * 預先列印
	 */
	public function advancePrint(User $user, array $params)
	{
		return MesQcPassMo::where('mac_id', $params['mac_id'])
			->where('mo_id', $params['mo_id'])
			->first() ? [
				'mac_id' => $params['mac_id'],
				'mo_id' => $params['mo_id'],
				'state' => 2,
				'qc' => $user->name,
				'qc_pass' => $params['qc_pass']
			] : false;
	}

	/**
	 * RFID TAG 狀態
	 */
	public function rfidTagStatus($rfids)
	{
		foreach ($rfids as $key => $rfid) {
			$data = DB::table('mes_registers')
						->where('mac_id', $rfid)
						->first();
			$rfid_info = DB::table('mes_registers')
						->where('mac_id', $rfid)
						->first();
			if ($data) {
				if ($rfid_info) {
					$status[] = [
						'mac_id' => $rfid,
						'rfid_status' => $data->rfid_status,
						'create_date' => date('Y-m-d', strtotime($data->created_at)),
						'record_date' => is_null($data->record_date) ? '' : $data->record_date,
						'mo_id' => $rfid_info->mo_id,
						'rfid_type' => $data->rfid_type
					];
				} else {
					$status[] = [
						'mac_id' => $rfid,
						'rfid_status' => $data->rfid_status,
						'create_date' => date('Y-m-d', strtotime($data->created_at)),
						'record_date' => is_null($data->record_date) ? '' : $data->record_date,
						'mo_id' => "",
						'rfid_type' => $data->rfid_type
					];
				}

			} else {
				$status[] = [
					'mac_id' => $rfid,
					'rfid_status' => 7,
					'create_date' => '',
					'record_date' => '',
					'mo_id' => '',
					'rfid_type' => ''
				];
			}
		}
		return $status;
	}

	/**
	 * 強制結案
	 */
	public function forceClosing($mo_id)
	{
		$data = [
			'status' => 1,	// 0:Red結案 1:強制結案
			'mo_id' => $mo_id,
			'mac_id' => '',		// RFID TAG
			'rfid_type' => 'B',		// Blue 尾數
			'rfid_status' => '',	// RFID 狀態
			'item' => $this->getItem($mo_id),	// 記錄製令料號
		];
		return MesMo::where('mo_id', $mo_id)
			->where('mo_status', '<>', 5)
			->first() ? $data : false;
	}

	/**
	 * 待確認
	 */
	public function getDateTime($mac_id, $color, $data)
	{
        $endTag = DB::table('mes_registers')
		        	->where('mac_id', $data['mac_id'])
		            ->where('rfid_type', 'R')
		            ->first();
        $blueTag = DB::table('mes_registers')
		        	->where('mac_id', $data['mac_id'])
		            ->where('rfid_type', 'B')
		            ->first();
        if ($color === 'B') {
	        if ($blueTag) {
	            $datetime = $data['now'];
	        } else {
	            $datetime = '';
	        }
        } elseif ($color === 'R') {
	        if ($endTag) {
	            $datetime = $data['now'];
	        } else {
	        	$datetime = '';
	        }
        } else {
        	$datetime = '';
        }

        return $datetime;
	}
}

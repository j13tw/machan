<?php

namespace App\Repositories;

use App\Entities\SetupSuspend;
use App\Entities\SetupException;
use App\Entities\SetupNonFinished;

class SetupRepository
{
    public function getNgIndex()
    {
        return SetupNonFinished::get();
    }

    public function getExceptionIndex()
    {
        return SetupException::get();
    }

    public function getSuspendIndex()
    {
        return SetupSuspend::get();
    }

    public function createUnfinished(array $data)
    {
        return SetupNonFinished::create($data);
    }

    public function updateUnfinished(array $data, $id)
    {
        $entity = SetupNonFinished::find($id);
        if (!$entity) {
            return 2;
        }
        return $entity->update($data) ? 0 : 1;
    }

    public function deleteUnfinished($id)
    {
        $entity = SetupNonFinished::find($id);
        if (!$entity) {
            return 2;
        }
        return $entity->destroy($id) ? 0 : 1;
    }

    public function createException(array $data)
    {
        return SetupException::create($data);
    }

    public function updateException(array $data, $id)
    {
        $entity = SetupException::find($id);
        if (!$entity) {
            return 2;
        }
        return $entity->update($data) ? 0 : 1;
    }

    public function destroyException($id)
    {
        $entity = SetupException::find($id);
        if (!$entity) {
            return 2;
        }
        return $entity->destroy($id) ? 0 : 1;
    }

    public function createSuspend(array $data)
    {
        return SetupSuspend::create($data);
    }

    public function updateSuspend(array $data, $id)
    {
        $entity = SetupSuspend::find($id);
        if (!$entity) {
            return 2;
        }
        return $entity->update($data) ? 0 : 1;
    }

    public function deleteSuspend($id)
    {
        $suspendInfo = SetupSuspend::find($id);
        if (!$suspendInfo) {
            return 2;
        }
        return $suspendInfo->destroy($id) ? 0 : 1;
    }
}
<?php

namespace App\Repositories;

use DB;
use App\Entities\User;
use App\Entities\MesRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPassMo;
use App\Entities\Notification;
use App\Entities\AppUnfinish;
use App\Entities\SetupLine;

class NotifyListRepository
{
	/**
	 * 依據通知代號 取得單一通知列表
	 */
    public function index($code, User $user)
    {
		$lineInfo = SetupLine::where('company_id', $user->company_id)
				->where('org_id', $user->org_id)
				->where('routing', $user->routing)
				->where('line_id', $user->line_id)
				->first();
        $lineName = $lineInfo->line_name;	// SetupTablet模型 關聯 SetupLine模型 取得線別名稱
		switch ($code) {
			case 'N001':
				return $this->peopleList($code, $lineName);
			
			case 'N002':
				return $this->changeList($code, $lineName);
			
			case 'N003':
				return $this->nonGreenList($code, $lineName);

			case 'N004':
				return $this->startList($code, $lineName);

			case 'N007':
				return $this->lastList($code, $lineName);

			case 'N008':
				return $this->endList($code, $lineName);

			case 'N009':
				return $this->nonStartList($code, $lineName);
			
			case 'N011':
				return $this->ngList($code, $lineName);

			case 'N016':
				return $this->forcePrintList($code, $lineName);

			case 'N017':
				return $this->reprintList($code, $lineName);

			case 'N018':
				return $this->susChangeList($code, $lineName);

			case 'N019':
				return $this->susWorkList($code, $lineName);
		}
    }

    // 顯示三天內通知 (含當日)
	// 開線人數通知 N001
	public function peopleList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N001',
						'mo_id' => $data->relatedMo->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'start_time' => $data->relatedMo->start_time,
						'customer_name' => $data->relatedMo->customer_name,
						'standard_human' => 18,
					];
				}
			})->toArray();
	}

	// 換模換線通知 N002
	public function changeList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N002',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_fin_date' => !is_null($data->relatedMo->finish_date) ? $data->relatedMo->finish_date : '',
						'mo_fin_time' => !is_null($data->relatedMo->finish_time) ? $data->relatedMo->finish_time : '',
						'change_time' => $data->relatedMo->change_time,
					];
				}
			})->toArray();
	}

	// 無Green開始 N003
	public function nonGreenList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N003',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'first_start_time' => $data->relatedMo->first_start_time,
					];
				}
			})->toArray();
	}

	// 最後一筆 N007
	public function lastList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$lastPro = $this->findLastProduct($data);
					return [
						'id' => $data->id,
						'type' => 'N007',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'qty' => $data->relatedMo->qty,
						'accumulator_fin' => $data->relatedMo->accumulator_fin,
						'pro_start_date' => $lastPro ? date('Y-m-d', strtotime($lastPro->start_datetime)) : '',
						'pro_start_time' => $lastPro ? date('H:i:s', strtotime($lastPro->start_datetime)) : ''
					];
				}
			})->toArray();
	}

	// 完工無開始 N009
	public function nonStartList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$finTime = $this->getProFinTime($data->mo_id, $data->mac_id);	// 取得產品完工時間
					return [
						'id' => $data->id,
						'type' => 'N009',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'human' => $data->relatedMo->acture_human === 0 ? 18 : $data->relatedMo->acture_human,
						'rfid_type' => $data->rfid_type,
						'mac_id' => $data->mac_id,
						'THT' => $data->relatedMo->tht,
						'finish_time' => $finTime,
						'es_start_time' => date('H:i:s', strtotime($finTime) - ($data->relatedMo->tht * $data->relatedMo->qty / 18)),
					];
				}
			})->toArray();
	}

	// 尾數異常 N011
	public function ngList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					return [
						'id' => $data->id,
						'type' => 'N011',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'qty' => $data->relatedMo->qty,
						'accumulator_start' => $data->relatedMo->accumulator_start,
						'accumulator_fin' => $data->relatedMo->accumulator_fin,
						'mo_fin_date' => $data->relatedMo->finish_date,
						'mo_fin_time' => $data->relatedMo->finish_time,
						'unfinishes' => $this->getNgTag(new AppUnfinish, $data->mo_id),
					];
				}
			})->toArray();
	}

	// QC PASS強制列印通知
	public function forcePrintList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$recordQty = MesRecord::where('mo_id', $data->mo_id)
								->where('rfid_type', 'B')
								->count();
					$forceInfo = $this->getForceQcPass($data->mo_id, $data->mac_id);
					return [
						'id' => $data->id,
						'type' => 'N016',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'qty' => $data->relatedMo->qty,
						'recordQty' => $recordQty,
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_start_qty' => $data->relatedMo->accumulator_start,	// 線別開工數
						'mo_fin_qty' => $data->relatedMo->accumulator_fin,	// 線別完工數 暫時取mes_mos 因為無法得知線別
						'print_count' => $this->getForceCount($data->mo_id),
						'qc_pass' => $forceInfo ? $forceInfo->qc_pass : '',
						'print_date' => $data->infor_date,
						'print_time' => $data->infor_time,
						'updated_at' => $data->updated_at
					];
				}
			})->toArray();
	}

	// 取得製令強制列印次數
	private function getForceCount($mo_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('force_print_time', '<>', null)
			->count();
	}

	// 取得製令強制列印之QC PASS
	private function getForceQcPass($mo_id, $mac_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('mac_id', $mac_id)
			->first();
	}

	// 重複列印 N017
	public function reprintList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				if ($data->relatedMo) {
					$recordQty = MesRecord::where('mo_id', $data->mo_id)
                                    ->where('rfid_type', 'B')
                                    ->count();
					$reprintData = $this->getReprintInfo($data);
					$qcpass = $reprintData === 1 ? [] : $reprintData;
					$batReprintInfo = $this->getBatchReprint(new Notification, $data->id);
					return [
						'id' => $data->id,
						'type' => 'N017',
						'customer_name' => $data->relatedMo->customer_name,
						'mo_id' => $data->mo_id,
						'item' => $data->relatedMo->item,
						'item_name' => '',
						'rfid_status' => $data->rfid_status,
						'mac_id' => $data->mac_id,
						'qty' => $data->relatedMo->qty,
						'recordQty' => $recordQty,
						'mo_start_date' => $data->relatedMo->date,
						'mo_start_time' => $data->relatedMo->start_time,
						'mo_fin_date' => $data->relatedMo->finish_date,
						'mo_fin_time' => $data->relatedMo->finish_time,
						'qc_pass' => count($qcpass) === 0 ? '' : $qcpass['qc_pass'],
						'print_date' => count($qcpass) === 0 ? '' : $qcpass['print_date'],
						'print_time' => count($qcpass) === 0 ? '' : $qcpass['print_time'],
						'reprint_date' => count($qcpass) === 0 ? $batReprintInfo->infor_date : $qcpass['reprint_date'],
						'reprint_time' => count($qcpass) === 0 ? $batReprintInfo->infor_time : $qcpass['reprint_time'],
					];
				}
			})->toArray();
	}

	// 開始區讀取異常 N004
	public function startList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				return [
					'id' => $data->id,
					'type' => 'N004',
					'mac_id' => $data->mac_id,
					'infor_date' => $data->infor_date,
					'infor_time' => $data->infor_time,
				];
			})->toArray();
	}

	// 完工區讀取異常 N008
	public function endList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				return [
					'id' => $data->id,
					'type' => 'N008',
					'mac_id' => $data->mac_id,
					'infor_date' => $data->infor_date,
					'infor_time' => $data->infor_time,
				];
			})->toArray();
	}

	// 除外工時 - 換模換線 N018
	public function susChangeList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				return [
					'id' => $data->id,
					'type' => 'N018',
					'infor_date' => $data->infor_date,
					'infor_time' => $data->infor_time,
				];
			})->toArray();
	}

	// 除外工時  作業效率異常 N019
	public function susWorkList($code, $lineName)
	{
		$datas = $this->selectEntity($code, $lineName);
		return
			$datas->map(function ($data) {
				return [
					'id' => $data->id,
					'type' => 'N019',
					'infor_date' => $data->infor_date,
					'infor_time' => $data->infor_time
				];
			})->toArray();
	}

	// 尋找最後一筆生產之產品
	public function findLastProduct($data)
	{
		return MesRfid::where('mo_id', $data->mo_id)
			->orderBy('start_datetime', 'DESC')
			->first();
	}

	// 成品完成時間
	private function getProFinTime($moid, $macid)
	{
		$data = MesRfid::where('mo_id', $moid)
					->where('mac_id', $macid)
					->first();
		return $data ? date('H:i:s', strtotime($data->end_datetime)) : '';
	}

	// 回傳該製令 未回覆尾數
	private function getNgTag(AppUnfinish $data, $moid)
	{
		return
			$data->where('mo_id', $moid)
				->get()
				->map(function ($item) {
					return $item->mac_id;
				})->filter()->values()->all();
	}

	// 尋找重複列印資訊
	private function getReprintInfo($data)
	{
		$reprint = MesQcPassMo::where('mo_id', $data->mo_id)
					->where('mac_id', $data->mac_id)
					->where('reprint', 1)
					->first();
		if ($reprint) {
			return [
				'qc_pass' => $reprint->qc_pass,
				'print_date' => $reprint->print_date,
				'print_time' => $reprint->print_time,
				'reprint_date' => $reprint->reprint_date,
				'reprint_time' => $reprint->reprint_time
			];
		}
		return 1;
	}

	// 回傳重複列印 (批次列印) 通知資訊
	private function getBatchReprint(Notification $entity, $id)
	{
		return $entity->find($id);
	}

	private function selectEntity($code, $lineName)
	{
        $diffDate = date('Y-m-d', strtotime('-2 days'));
		return Notification::where('line_name', $lineName)
            ->where('code', $code)
            ->whereBetween(DB::raw('date(created_at)'), [$diffDate, date('Y-m-d')])
            ->orderBy('created_at', 'DESC')
            ->get();
	}
}
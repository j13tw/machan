<?php

namespace App\Console\Commands;

use DB;
use Redis;
use Illuminate\Console\Command;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesMoRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPass;
use App\Entities\MesQcPassMo;
use App\Entities\MesRegister;
use App\Entities\Notification;
use App\Entities\SetupNotification;

class PrintQcPassSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:PrintQcPassSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['qcpass-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    $qcpassTables = [new MesQcPass, new MesQcPassMo];
                    $rfidTables = [new MesRfid, new MesMoRfid];

                    // 檢查一般列印 or 重複列印 or 強制列印
                    $checkPrint = MesQcPass::where('qc_pass', $data->qc_pass)
                                    ->where('mac_id', $data->mac_id)
                                    ->where('printed', 1)
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
                    $qcData = MesQcPass::where('mo_id', $data->mo_id)
                                ->where('mac_id', $data->mac_id)
                                ->orderBy('created_at', 'DESC')
                                ->first();
                    $qcMoData = MesQcPassMo::where('mo_id', $data->mo_id)
                                ->where('mac_id', $data->mac_id)
                                ->orderBy('created_at', 'DESC')
                                ->first();

                    // 一般列印 變更完工狀態
                    if ($data->state == 0) {
                        // 變更列印狀態
                        $printInfo = [
                            'print_date' => date('Y-m-d'),
                            'print_time' => date('H:i:s'),
                            'printed' => 1,
                        ];
                        $this->modifyPrintStatus($data->mo_id, $data->mac_id, $printInfo, $qcpassTables);

                        $finQty = MesQcPassMo::where('mo_id', $data->mo_id)
                                    ->where('rfid_type', 'B')
                                    ->where('rfid_status', 4)
                                    ->where('printed', 1)
                                    ->count();
                        $moData = MesMo::where('mo_id', $data->mo_id)
                                    ->first();
                        if ($moData->finish === 1) {    // 一次通過率 製令未完工才計算
                            $moData->update(['accumulator_fin' => $finQty]);
                        } else {
                            $accuStart = $moData->accumulator_start;
                            $passRate = $accuStart !== 0 ? round(($finQty / $accuStart), 2) * 100 . '%' : '0%';
                            $moData->update([
                                'accumulator_fin' => $finQty,
                                'first_pass_rate' => $passRate
                            ]);
                        }

                        // 完工數與訂單數相同時 直接更新完工時間
                        if ($moData->qty == $moData->accumulator_fin) {

                            $startQty = $this->getFinishStatus($data->mo_id)->where('start_datetime', '<>', null)->get();
                            $endQty = $this->getFinishStatus($data->mo_id)->where('rfid_status', 4)->get();
                            $nfQty = $this->getFinishStatus($data->mo_id)->where('rfid_status', '<>', 4)->get();
                            if ($moData->nf_qty == 0) {
                                $moData->update(['nf_qty' => count($nfQty)]);
                            }
                            $moData->update([
                                'finish_date' => date('Y-m-d'),
                                'finish_time' => date('H:i:s'),
                                'finish' => 1,
                                'first_finish_date' => date('Y-m-d'),
                                'first_finish_time' => date('H:i:s'),
                                'prod_qty' => count($startQty),
                                'fin_qty' => count($endQty),
                                'nf_qty' => count($nfQty)
                            ]);
        
                            foreach ($rfidTables as $key => $table) {
                                $finishInfo = [
                                    'finish' => 1,
                                    'finish_time' => date('H:i:s'),
                                ];
                                $table->where('mo_id', $data->mo_id)
                                    ->update($finishInfo);
                            }

                            // 製令完工狀態
                            $moData = MesMo::where('mo_id', $data->mo_id)
                                        ->first();
                            if ($moData->nfQty == 0) {
                                $moData->update(['mo_status' => 5]);    // 無尾數完工
                            } else {
                                $moData->update(['mo_status' => 3]);    // 有尾數完工
                            }
                            // TCT TCT(HR) CT
                            $startTime = $moData->date.$moData->start_time;
                            $endTime = $moData->finish_date.$moData->finish_time;
                            $tct = floor(strtotime($endTime) - strtotime($startTime));
                            $moData->update([
                                'tct' => $tct,
                                'ct' => $tct / $moData->fin_qty
                            ]);
                        }
                    }

                    if (is_null($checkPrint)) {
                        $printInfo = [
                            'print_date' => date('Y-m-d'),
                            'print_time' => date('H:i:s'),
                            'printed' => 1,
                        ];
                        $this->modifyPrintStatus($data->mo_id, $data->mac_id, $printInfo, $qcpassTables);
                    } else {
                        // 重複列印
                        if ($data->state == 1) {
                            $printInfo = [
                                'reprint_date' => date('Y-m-d'),
                                'reprint_time' => date('H:i:s'),
                                'reprint' => 1,
                                'printed' => 1,
                            ];
                            $this->modifyPrintStatus($data->mo_id, $data->mac_id, $printInfo);
                        }
                        $this->notifyReprint($data);
                    }
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }

    // 取得該製令 當批投入數 完工數 尾數
    private function getFinishStatus($mo_id)
    {
        return MesRfid::where('mo_id', $mo_id)
            ->where('rfid_type', 'B');
    }

    // 更新列印狀態 (一般列印 重複列印)
    private function modifyPrintStatus($mo_id, $mac_id, $printInfo, $tables)
    {
        foreach ($tables as $key => $table) {
            $table->where('mo_id', $mo_id)
            ->where('mac_id', $mac_id)
            ->orderBy('created_at', 'DESC')
            ->first()
            ->update($printInfo);
        }
    }

    // QC PASS重複列印通知
    public function notifyReprint($data)
    {
        $code = 'N017';     // 重複列印通知代號
        $dataTmp = $this->dataCollection($code, $data);
        Notification::create($dataTmp);
    }

    // 待寫入資料集合
    public function dataCollection($code, $data)
    {
        $notiInfo = SetupNotification::where('code', $code)
                        ->first();
        $rfidData = MesQcPassMo::where('mo_id', $data->mo_id)
                        ->where('mac_id', $data->mac_id)
                        ->where('qc_pass', $data->qc_pass)
                        ->first();
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        return [
            'code' => $code,
            'infor_date' => date('Y-m-d'),
            'infor_time' => date('H:i:s'),
            'role_name' => $notiInfo->role_name,
            'mac_id' => $data->mac_id,
            'rfid_type' => $data->rfid_type,
            'company_id' => $rfidData->company_id,
            'factory_id' => $rfidData->factory_id,
            'rfid_status' => $rfidData->rfid_status,
            'mo_id' => $data->mo_id,
            'item' => $data->item,
            'qty' => $rfidData->qty,
            'completion_date' => $rfidData->completion_date,
            'so_id' => $rfidData->so_id,
            'customer_id' => $rfidData->customer_id,
            'customer_name' => $rfidData->customer_name,
            'line_id' => $rfidData->line_id,
            'line_name' => $rfidData->line_name
        ];
    }
}

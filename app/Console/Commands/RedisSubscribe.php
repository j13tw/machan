<?php

namespace App\Console\Commands;

use Redis;
use Illuminate\Console\Command;
use App\Entities\MesRegister;

class RedisSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:RedisSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['register-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    $infos = $data->mac_id;
                    $rfidInfo = [];
                    foreach ($infos as $key => $info) {
                        $rfidInfo[] = [
                            'mac_id' => $info,
                            'rfid_type' => $data->type,
                            'company_id' => $data->company_id,
                            'factory_id' => $data->factory_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ];
                    }
                    MesRegister::insert($rfidInfo);
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }
}


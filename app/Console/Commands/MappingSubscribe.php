<?php

namespace App\Console\Commands;

use DB;
use Redis;
use Illuminate\Console\Command;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesMoRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPass;
use App\Entities\MesQcPassMo;
use App\Entities\MesRegister;
use App\Entities\SetupTablet;

class MappingSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:MappingSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['mapping-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    $infos = $data->mac_data;
                    $recordId = $data->mo_id.'13'.date('H').date('m');
                    foreach ($infos as $key => $info) {
                        $tagData = MesRegister::where('mac_id', $info)
                                    ->first();
                        MesRecord::create([
                            'record_date' => date('Y-m-d'),
                            'record_id' => $recordId,
                            'complete_date' => $data->complete_date,
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'mo_qty' => $data->mo_qty,
                            'mac_id' => $info,
                            'rfid_type' => $tagData->rfid_type,
                            'rfid_status' => 1,
                            'company_id' => $tagData->company_id,
                            'factory_id' => $tagData->factory_id,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                        ]);
                        MesRfid::create([
                            'mac_id' => $info,
                            'record_id' => $recordId,
                            'rfid_type' => $tagData->rfid_type,
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                            'rfid_status' => 1,
                        ]);
                        MesMoRfid::create([
                            'mac_id' => $info,
                            'rfid_type' => $tagData->rfid_type,
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'qty' => $data->mo_qty,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                            'rfid_status' => 1,
                        ]);
                        MesQcPass::create([
                            'qc_year' => date('Y'),
                            'mac_id' => $info,
                            'rfid_type' => $tagData->rfid_type,
                            'record_id' => $recordId,
                            'company_id' => $tagData->company_id,
                            'factory_id' => $tagData->factory_id,
                            'rfid_status' => 1,
                            'record_date' => date('Y-m-d H:i:s'),
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'qty' => $data->mo_qty,
                            'completion_date' => $data->completion_date,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                            'org_id' => $tagData->factory_id,
                        ]);
                        MesQcPassMo::create([
                            'mac_id' => $info,
                            'rfid_type' => $tagData->rfid_type,
                            'rfid_status' => 1,
                            'record_id' => $recordId,
                            'company_id' => $tagData->company_id,
                            'factory_id' => $tagData->factory_id,
                            'record_date' => date('Y-m-d H:i:s'),
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'qty' => $data->mo_qty,
                            'completion_date' => $data->completion_date,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                            'org_id' => $tagData->factory_id,
                        ]);
                        $tagData->update([
                            'rfid_status' => 1,
                            'record_date' => date('Y-m-d')
                        ]);

                        // 將製令單號回寫至mes_registers
                        MesRegister::where('mac_id', $info)
                            ->update(['mo_id' => $data->mo_id]);
                    }

                    $moData = MesMo::where('mo_id', $data->mo_id)
                                ->first();
                    if (!$moData) {
                        MesMo::create([
                            'record_date' => date('Y-m-d'),
                            'record_id' => $recordId,
                            'mo_id' => $data->mo_id,
                            'item' => $data->item,
                            'qty' => $data->mo_qty,
                            'completion_date' => $data->completion_date,
                            'so_id' => $data->so_id,
                            'customer_id' => $data->customer_id,
                            'customer_name' => $data->customer_name,
                            'tht' => $data->tht
                        ]);
                    }
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoSeven extends Model
{
    protected $fillable = [
        'mo_status', 'mo_id',
        'separate', 'factory_id', 'line_id', 'group_id', 'group_qty',
        'start_line', 'start_date', 'start_time', 'finish_date', 'finish_time',
        'rest_time', 'accumulator_start', 'accumulator_fin', 'finish',
        'change_time', 'ng_qty', 'ng_reason', 'ng_start_time', 'ng_finish_time',
        'suspend_reason', 'exp_change_time', 'suspend',
        'acture_human', 'target_tht', 'target_ht', 'bottle_ct', 'ht', 'first_pass_rate',
        'entry_register', 'cross_sum_tct',
    ];
}

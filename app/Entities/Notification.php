<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'code', 'notification_id', 'message', 'title',
        'content', 'infor_date', 'infor_time', 'name',
        'role_name', 'reply', 'reply_date', 'reply_time',
        'sender', 'receiver', 'mac_id', 'rfid_type',
        'company_id', 'factory_id', 'rfid_status',
        'date', 'mo_id', 'item', 'qty', 'completion_date',
        'so_id', 'customer_id', 'customer_name', 'org_id',
        'mo_status', 'line_id', 'line_name', 'routing',
        'record_id',
    ];

    public function relatedMo()
    {
        return $this->belongsTo('App\Entities\MesMo', 'mo_id', 'mo_id');
    }

    public function relatedApp()
    {
        return $this->hasMany('App\Entities\AppUnfinish', 'mo_id', 'mo_id');
    }

    public function relatedSuspend()
    {
        return $this->hasMany('App\Entities\AppSuspend', 'mo_id', 'mo_id');
    }
}

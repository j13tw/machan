<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupEboard extends Model
{
    protected $fillable = [
    	'dashboard_id', 'dashboard_name', 'type', 'purpose',
    	'line_id', 'org_id', 'routing', 'profile', 'note'
    ];
}

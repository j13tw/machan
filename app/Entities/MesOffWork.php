<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesOffWork extends Model
{
    protected $fillable = [
    	'profile', 'line_id', 'routing', 'off_work_date',
    	'off_work_time', 'mac_id', 'rfid_type', 'rfid_status',
    	'tablet_id', 'mo_id', 'item', 'mo_qty', 'mo_status',
    	'mo_start_date', 'mo_start_time', 'tht', 'finish',
    	'ng', 'human', 'acture_human', 'start_line',
    	'start_profile', 'start_antenna_id', 'start_date',
    	'start_time', 'first_one_time', 's_tct', 'serial_no',
    	'change_date', 'change_time', 'change_line_id',
    	'line_name', 'change_profile', 's_cl_start_time',
    	's_cl_end_time', 's_change_lead_time', 's_acture_human',
    	'cl_start_time', 'cl_end_time', 'change_lead_time'
    ];
}
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupRole extends Model
{
    protected $fillable = [
    	'role_id', 'role_name', 'active', 'note'
    ];
}
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderThree extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'mixed_mo_id', 'mo_id', 'group_id',
        'mix_start_date', 'mix_start_time', 'mix_finish_date', 'mix_finish_time',
        'accu_insert_finish', 'mix_rest_time', 'cl_start_time', 'cl_end_time',
        'insert_change_time', 'mix_sum_tct',
    ];
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupOrg extends Model
{
    protected $fillable = [
    	'time', 'accumulator_time'
    ];
}

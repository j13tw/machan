<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoBodyFive extends Model
{
    protected $fillable = [
        'rfid_status', 'tag_id', 'mac_id', 'mo_id',
        'start_date', 'start_time', 'finish_date', 'finish_time',
    ];
}

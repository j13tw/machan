<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EbDayLine extends Model
{
    protected $fillable = [
    	'line_id', 'company_id', 'org_id', 'routing',
    	'dashboard_id', 'profile', 'status', 'eb_qty',
    	'line_mo_qty', 'rank', 'day_accumulator_start',
    	'day_accumulator_fin', 'day_ng', 'day_qty',
    	'day_first_pass_rate', 'day_change_lead_time',
    	'day_suspend_time', 'mo_start_time', 'mo_finish_time',
    	'finish', 'prod_qty', 'fin_qty', 'nf_qty',
    	'change_lead_time', 'cl_start_time', 'cl_end_time',
    	'first_pass_rate', 'acture_human', 'tht', 'ht',
    	'tct', 'ct', 'qty_per_hr', 'productivity',
    	'reach_rate', 'f_completion_time', 'target_qty',
    	'suspend_rate', 'std_change_lead', 'equivalents',
    	'date', 'mo_id', 'item', 'qty', 'completion_date',
    	'so_id', 'customer_id', 'customer_name',
    	'mo_status', 'accumulator_start', 'accumulator_fin',
    	'immediate_tct', 'immediate_ct', 'immediate_qty_per_hr',
    	'immediate_productivity', 'capacity_rate', 'color',
    	'immediate_equivalents'
    ];
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesRegister extends Model
{
    protected $fillable = [
    	'mac_id', 'tag_id', 'mo_id', 'rfid_type', 'company_id', 'factory_id',
    	'rfid_status', 'record_date', 'erase_date',
    	'disable_date', 'enable',
	];

	public function relatedRfid()
	{
		return $this->hasMany('App\Entities\MesRfid', 'mac_id', 'mac_id');
	}
	
	public function relatedMo()
	{
		return $this->belongsTo('App\Entities\MesMo', 'mo_id', 'mo_id');
	}
}
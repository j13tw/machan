<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesTimeflag extends Model
{
    protected $fillable = [
    	'profile', 'type', 'date', 'time', 'cl_end_time',
    	'cl_start_time', 'mo_id', 'change_time', 'note'
    ];
}
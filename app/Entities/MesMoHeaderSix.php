<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderSix extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'mo_status', 'mo_id', 'group_id', 'delta_t',
        'cross_start_date', 'cross_start_time', 'cross_finish_date', 'cross_finish_time',
        'prod_qty', 'fin_qty', 'rest_time', 'accu_rest_time', 'cl_start_time', 'cl_end_time',
        'cross_change_time', 'accu_change_time', 'con_sum_tct', 'non_con_delta_tct',
    ];
}

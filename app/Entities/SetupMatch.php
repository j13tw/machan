<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupMatch extends Model
{
    protected $fillable = [
    	'profile', 'org_id', 'routing', 'line_id',
    	'antenna_id', 'antenna_name', 'antenna_type',
    	'port_no', 'port_id', 'status_at', 'reader_id',
    	'reader_name', 'reader_ip', 'status_rd',
    	'tablet_id', 'ip', 'person_id', 'name',
    	'status_tb', 'b_print_id', 'b_print_name',
    	'status_bp', 'dashboard_id', 'dashboard_name',
    	'status_db', 'server', 'server_ip', 'status_sv',
    	'note'
    ];
}

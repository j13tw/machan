<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesUnfinish extends Model
{
    protected $fillable = [
    	'code_nf', 'red_mac_id', 'rfid_type', 'nf_type',
    	'mo_qty', 'rt_end_line', 'rt_end_profile',
    	're_antenna_id', 'rt_end_date', 'rt_end_time',
    	'nf_status', 'mac_id', 'record_id',
    	'date', 'mo_id', 'item', 'so_id', 'customer_id',
    	'customer_name', 'start_line', 'start_profile',
    	'start_antenna_id', 'start_date', 'end_line',
    	'end_profile', 'end_antenna_id', 'end_date',
    	'end_time', 'qc_pass', 'qc', 'finish',
    	'finish_time', 'ng', 'except_reason',
    	'ex_start_time', 'ex_end_time', 'suspend',
    	'mo_status', 'code', 'notification_id',
    	'ntf_data_time', 'notification',
    	'rfid_status', 'dashboard_id', 'remark'
    ];
}

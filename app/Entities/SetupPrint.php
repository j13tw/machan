<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupPrint extends Model
{
	protected $fillable = [
		'b_print_id', 'b_print_name', 'tablet_id',
		'reader_id', 'line_id', 'org_id', 'routing',
		'profile', 'note'
	];
}
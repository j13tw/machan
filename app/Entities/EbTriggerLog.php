<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EbTriggerLog extends Model
{
    protected $fillable = [
    	'tg_date', 'tg_time', 'trigger_id', 'trigger_name',
    	'dashboard_id', 'dashboard_name', 'type', 'line_id',
    	'org_id', 'routing', 'profile', 'status', 'mac_id',
    	'rfid_type', 'record_id', 'company_id', 'rfid_status',
    	'priority', 'refresh_trigger', 'close', 'close_trigger',
    	'note'
    ];
}
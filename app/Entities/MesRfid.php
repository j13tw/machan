<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesRfid extends Model
{
    protected $fillable = [
    	'mac_id', 'record_id', 'rfid_type', 'rfid_status', 'mo_id',
    	'item', 'so_id', 'customer_id', 'customer_name',
    	'start_line', 'start_profile', 'start_antenna_id',
    	'start_datetime', 'end_line', 'end_profile',
    	'end_antenna_id', 'end_datetime', 'qc_pass',
    	'qc', 'finish', 'finish_time', 'ng', 'exception_reason',
    	'ex_start_time', 'ex_end_time', 'suspend',
    	'notification_id', 'ntf_date_time', 'notification',
    	'remark',
    ];

    public function relatedMo()
    {
        return $this->belongsTo('App\Entities\MesMo', 'mo_id', 'mo_id');
	}

	public function relatedRegister()
	{
		return $this->belongsTo('App\Entities\MesRegister', 'mac_id', 'mac_id');
	}
}
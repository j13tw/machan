<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SetupAccount extends Authenticatable
{
	use Notifiable;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'account', 'password', 'sync', 'employee_id',
    	'name', 'company_id', 'org_id', 'routing',
    	'role_id', 'role_name', 'active', 'note'
	];
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		
    ];

    public function relatedEquip()
    {
        return $this->hasOne('App\Entities\SetupEquip', 'person_id', 'account');
    }
}
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderFive extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'mo_status', 'mo_id', 'group_id', 'delta_t',
        'cross_start_date', 'cross_start_time', 'cross_finish_date', 'cross_finish_time',
        'prod_qty', 'fin_qty', 'rest_time', 'accu_rest_time', 'cl_start_time', 'cl_end_time',
        'sep_change_time', 'insert_change_time', 'sep_sum_tct', 'non_sep_delta_tct',
    ];
}

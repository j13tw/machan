<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AppStart extends Model
{
    protected $fillable = [
    	'code_start', 'type', 'profile', 'line_id', 'org_id',
    	'routing', 'person_id', 'name', 'role_id', 'human',
    	'acture_human', 'mo_id', 'item', 'mo_qty', 'mac_id',
    	'rfid_type', 'rfid_status', 'start_line', 'start_profile',
    	'start_datetime', 'first_one_time',
    	'mo_start_datetime', 'end_line', 'end_profile',
    	'end_antenna_id', 'end_datetime', 'tht',
    	'change_date', 'change_time', 'change_line_id', 'line_name',
    	'change_profile', 'serial_no', 'cl_start_time', 'cl_end_time',
    	'change_lead_time'
    ];
}
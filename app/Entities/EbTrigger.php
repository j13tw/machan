<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EbTrigger extends Model
{
    protected $fillable = [
    	'trigger_id', 'trigger_name', 'module', 'action_description',
    	'judgement_description', 'eb_qty', 'line_mo_qty', 'priority',
    	'rank', 'refresh_trigger', 'close', 'close_trigger',
    	'start_eb_rank', 'line_mo_rank', 'end_eb_rank',
    	'note'
    ];
}
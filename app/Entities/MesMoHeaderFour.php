<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderFour extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'mo_status', 'mo_id', 'group_id', 'delta_t',
        'ng_start_date', 'ng_start_time', 'ng_finish_date', 'ng_finish_time',
        'accu_ng_fin_qty', 'ng_delta_tct',
    ];
}

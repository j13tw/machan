<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupAntenna extends Model
{
    protected $fillable = [
    	'antenna_id', 'antenna_name', 'antenna_type',
    	'port_no', 'port_id', 'device_id', 'profile',
    	'note', 'org_id', 'routing', 'line_id',
    ];

    public function relatedLine()
    {
        return $this->belongsTo('App\Entities\SetupLine', 'profile', 'profile');
    }
}
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoTen extends Model
{
    protected $fillable = [
        'sep_datetime', 'factory_id', 'line_id', 'mo_status', 'mo_id',
        'group_id', 'group_qty', 'person', 'item', 'qty', 'completion_time',
        'so_id', 'customer_id', 'customer_name',
    ];
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EbWeb extends Model
{
    protected $fillable = [
    	'company_id', 'org_id', 'routing', 'type',
    	'line_id', 'dashboard_id', 'profile', 'status',
    	'eb_qty', 'line_mo_qty', 'rank', 'day_mo_qty',
    	'day_accumulator_start', 'day_accumulator_fin',
    	'day_ng', 'day_qty', 'day_first_pass_rate',
    	'day_change_lead_time', 'day_suspend_time',
    	'object_equivalents', 'accu_fin_equivalents',
    	'capacity_rate', 'color'
    ];
}
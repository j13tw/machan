<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EbMo extends Model
{
    protected $fillable = [
    	'dashboard_id', 'dashboard_name', 'status', 'eb_qty',
    	'line_mo_qty', 'rank', 'type', 'line_id', 'org_id',
    	'routing', 'profile', 'date', 'mo_id', 'qty',
    	'completion_date', 'so_id', 'customer_id',
    	'customer_name', 'mo_status', 'accumulator_start',
    	'accumulator_fin', 'ng', 'mo_start_time',
    	'mo_finish_date', 'mo_finish_time', 'finish',
    	'start_time', 'finish_date', 'finish_time',
    	'rest_time', 'prod_qty', 'fin_qty', 'nf_qty',
    	'change_lead_time', 'cl_start_time', 'cl_end_time',
    	'nex_id', 'suspend_time', 'first_pass_rate',
    	'acture_human', 'tht', 'ht', 'tct', 'ct',
    	'qty_per_hr', 'productivity', 'reach_rate',
    	'f_completion_time', 'target_qty', 'suspend_rate',
    	'std_change_lead', 'equivalents', 'immediate_tct',
    	'immediate_ct', 'immediate_qty_per_hr',
    	'immediate_productivity', 'capacity_rate',
    	'color', 'immediate_equivalents'
    ];
}

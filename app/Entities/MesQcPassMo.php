<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesQcPassMo extends Model
{
    protected $fillable = [
    	'mac_id', 'record_id', 'rfid_type', 'rfid_status',
    	'company_id', 'factory_id', 'end_line', 'end_profile',
    	'date', 'end_antenna_id', 'end_datetime',
    	'mo_id', 'item', 'qty', 'completion_date', 'so_id',
    	'customer_id', 'customer_name', 'mo_status', 'line_id',
    	'line_name', 'org_id', 'routing', 'profile', 'dashboard_id',
    	'dashboard_name', 'accumulator_qty', 'accumulator_non_print',
    	'qc_pass', 'qc', 'print_date', 'print_time', 'force_print_date',
    	'force_print_time', 'reprint_date', 'reprint_time', 'printed',
    	'force_print', 'reprint',
    ];

    public function relatedMo()
    {
        return $this->belongsTo('App\Entities\MesMo', 'mo_id', 'mo_id');
    }

    public function relatedStart()
    {
        return $this->hasOne('App\Entities\MesStart', 'mac_id', 'mac_id');
    }
}
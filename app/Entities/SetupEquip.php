<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupEquip extends Model
{
    protected $fillable = [
    	'person_id', 'name', 'role_id', 'role_name',
    	'tablet_id', 'line_id', 'org_id', 'routing',
    	'profile', 'note'
    ];
}
<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'account', 'email', 'password',
        'company_id', 'org_id', 'routing',
    	'line_id', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // 取得綁線別人員相關資訊 (未綁定者待修)
    public function profileScope($line_id)
    {
        return $this->hasMany('App\Entities\SetupReader', 'org_id', 'org_id')->where('line_id', $line_id);
    }
}
